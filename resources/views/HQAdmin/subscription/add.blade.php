@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="">
                <a href="{{url('/super-admin/status/list')}}" class="btn btn-success">List Status</a>
            </div>

        </div>
    </div>
    <br>
    <div class="panel panel-default">
        <div class="panel-heading">

            <h4 class="panel-title">Add New Status</h4>

        </div>
        <div class="panel-body">
            {{Form::open(['url'=>'super-admin/status/save','method'=>'POST'])}}
            <div class="row">
                <div class="col-xs-8">
                    {!! Form::label('name','Status Name') !!}
                    {!! Form::text('status_name',null,(['class'=>'form-control','id'=>'status_name'])) !!}
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-8">
                    {!! Form::label('name','Status Code') !!}
                    {!! Form::text('status_code',null,(['class'=>'form-control','id'=>'status_code'])) !!}
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-5">
                    <button class="btn btn-success"><i class="glyphicon glyphicon-floppy-save"></i> Save Changes
                    </button>
                </div>
            </div>
            {{Form::close()}}
        </div>
    </div>
@endsection