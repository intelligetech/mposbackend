@extends('layouts.admin')
@section('content')
    <div class="modal  fade" tabindex="-1" role="dialog" id="delete">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Delete Employee</h4>
                </div>
                {!! Form::open(['url'=>'delete/user','method'=>'GET']) !!}
                <div class="modal-body">
                    <input hidden id="del" name="id">
                    <p>Are you sure you want to delete this employee &hellip;</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-danger">Delete</button>
                </div>
                {!! Form::close() !!}
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Subscription</h4>
                </div>
                <div class="modal-body">

                    <div class="modal-body__container">
                        <h4>Details</h4>
                        <p>You are about to subscribe to the yearly package of Ksh.{{$billingCycle->amount}}</p>
                        <p>Payments are to be done via Mpesa. Once you click subscribe a prompt up will appear on your
                            phone
                            to make the payment</p>
                        <p>Payment is expected on this number: <strong> {{Auth::user()->phone_number}}</strong>
                        </p>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="flat" id="otherNumberSubscribe"> Use other
                                Number?
                            </label>
                        </div>
                        <input type="text" name="phone_number" id="phone_numberSubscribe"
                               class="form-control hidden phone-number"
                               minlength="12" maxlength="12" placeholder="2547********">
                        <p>Do you wish to continue? </p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="subscribe" data-dismiss="modal">Subscribe
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade activate-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">Subscription</h4>
                </div>
                <div class="modal-body">

                    <div class="modal-body__container">
                        <h4>Details</h4>
                        <p>You are about to subscribe to the yearly package of Ksh.{{$billingCycle->amount}}</p>
                        <p>Payments are to be done via Mpesa. Once you click subscribe a prompt up will appear on your
                            phone
                            to make the payment</p>
                        <p>Payment is expected on this number: <strong> {{Auth::user()->phone_number}}</strong>
                        </p>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="flat" id="otherNumberActivate"> Use other
                                Number?
                            </label>
                        </div>
                        <input type="text" name="phone_number" id="phone_numberActivate"
                               class="form-control hidden phone-number"
                               minlength="12" maxlength="12" placeholder="2547********">
                        <p>Do you wish to continue? </p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="confirmActivate" data-dismiss="modal">Subscribe
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
            {{--<div class="row">
                <div class="col-lg-6">
                    <div class="">
                        <a href="{{url('/super-admin/status/add')}}" class="btn btn-success">Add Status</a>
                    </div>

                </div>
            </div>
            <br>--}}
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Subscription
                            </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                                <div class="profile_img">
                                    <div id="crop-avatar">
                                        <!-- Current avatar -->
                                        <img class="img-responsive avatar-view" src="{{url('images/user.png')}}"
                                             alt="Avatar" title="Change the avatar">
                                    </div>
                                </div>
                                <h3>{{Auth::user()->username}}</h3>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-12">

                                <div class="profile_title">
                                    <div class="col-md-6">
                                        <h2>My Subscription Plan</h2>
                                    </div>
                                </div>
                                <div class="profile_description">
                                    @if(count($existingSubscription))
                                        <div class="profile_content">


                                            @if($existingSubscription->code == 'SUBPENDING')

                                                <p>You have subscribed to the <strong>Premium</strong> Subscription but
                                                    your payment is pending</p>
                                                <p>Subscription Date: {{$existingSubscription->created_at}}
                                                </p>
                                                <div class="subscription-btns">
                                                    <button type="button" class="btn btn-primary" id="activateBtn">
                                                        Activate
                                                    </button>
                                                    <button type="button" class="btn btn-primary">Cancel Subscription
                                                    </button>
                                                </div>
                                            @elseif($existingSubscription->code == 'SUBACTIVE')
                                                <p>You are currently on the <strong>Premium</strong> Subscription</p>
                                                <p>Subscription Date: {{$existingSubscription->created_at}}
                                                </p>
                                                @if(count($payment))
                                                    <p>Expiry Date:
                                                        {{$payment->expiry_date}}</p>
                                                @endif()
                                            @elseif($existingSubscription->code == 'SUBCANCELLED')
                                            @elseif($existingSubscription->code == 'SUBEXPIRED')
                                            @elseif($existingSubscription->code == 'SUBSUSPENDED')

                                            @endif()

                                        </div>
                                    @else()
                                        <div class="alert alert-warning alert-dismissible fade in" role="alert">
                                            <strong>Sorry!</strong> You are currently not subscribed
                                        </div>

                                        <div class="package">
                                            <div class="package__title">
                                                <h2>Premium <strong>Package</strong></h2>
                                            </div>
                                            <hr>
                                            <div class="package__pricing">
                                                <sup>KES</sup>
                                                <span class="package__money">{{number_format( $billingCycle->amount)}}
                                                    /-</span>
                                                <small>Per Year</small>
                                            </div>
                                            <div class="package__content">
                                                <ul>
                                                    <li>
                                                        Support Unlimited Products for your business
                                                    </li>
                                                    <li>
                                                        End to end backup of your shops' data
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12" style="text-align: center">
                                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                                        data-target=".bs-example-modal-sm">Subscribe
                                                </button>
                                            </div>
                                        </div>
                                    @endif()
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="x_panel hidden">
                <div class="x_title">
                    <h2>List Subscription
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if(!count($existingSubscription))

                    @else()
                        <div class="row">
                            <div class="col-lg-4">
                                Premium Subscription
                            </div>
                            <div class="col-lg-4">
                                <p>Subscription Date:</p>
                                {{$existingSubscription->created_at}}
                            </div>
                            <div class="col-lg-4">
                                @if($existingSubscription->code == 'SUBPENDING')
                                    <div class="subscription-btns">
                                        <button type="button" class="btn btn-primary">Activate</button>{{--
                                        <button type="button" class="btn btn-primary">Cancel Subscription</button>--}}
                                    </div>
                                @elseif($existingSubscription->code == 'SUBACTIVE')
                                    @if(count($payment))
                                        <p>Expiry Date:
                                            {{$payment->expiry_date}}</p>
                                    @endif()
                                @elseif($existingSubscription->code == 'SUBCANCELLED')
                                @elseif($existingSubscription->code == 'SUBEXPIRED')
                                @elseif($existingSubscription->code == 'SUBSUSPENDED')

                                @endif()
                            </div>
                        </div>
                    @endif()
                    {{--
                                        {!! $dataTable->table(['class' => 'table table-striped responsive','style'=>"width:100%"]) !!}
                    --}}
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts'){{--
    <script src="{{url('vendors/iCheck/icheck.min.js')}}"></script>--}}

<script>
    $(function () {
        $("#delete").on('show.bs.modal', function (e) {
            ID = $(e.relatedTarget).attr('id');
            $("#del").val(ID);
        })
    });

    $('#subscribe').on('click', function (e) {
        console.log($(e.currentTarget).attr('data-dismiss'));
        var phone_number = $('#phone_numberSubscribe').val();
        console.log(phone_number);
        var user_id = {{Auth::user()->id}};
        console.log(user_id);


        $.ajax({
            url: url + '/hq-admin/subscription/add',
            method: 'POST',
            data: {
                "_token": "{{ csrf_token() }}",
                "phone_number": phone_number
            },
            success: function (data) {
                console.log(data);
                toastr.success('Subscription Added');
                window.location.href = url + "/hq-admin/subscription/list";
            },
            error: function (err) {
                console.log(err);

                if (err.custom) {
                    toastr.error("An Error occurred. Failed to add subscription. " + err.custom)
                } else {
                    toastr.error("An Error occurred. Failed to add subscription");
                }
            }
        })
        ID = $(e.relatedTarget).attr('data-dismiss');
        console.log(ID);
    });

    /*activate subscription*/
    $('#activateBtn').on('click', function (e) {
        //  console.log($(e.currentTarget).attr('data-dismiss'));

        var user_id = {{Auth::user()->id}};
        console.log(user_id);


        $.ajax({
            url: url + '/hq-admin/subscription/check',
            method: 'POST',
            data: {
                "_token": "{{ csrf_token() }}"
            },
            success: function (data) {
                console.log(data);
                toastr.success('Subscription Added');
                window.location.href = url + "/hq-admin/subscription/list";
            },
            error: function (err) {
                console.log(err);
                //  toastr.error("An Error occurred. Failed to add subscription")
            },
            statusCode: {
                400: function () {
                    $('.activate-modal').modal('show');
                }
            }
        });
        ID = $(e.relatedTarget).attr('data-dismiss');
        console.log(ID);
    });

    /*confirm activation*/
    $('#confirmActivate').on('click', function (e) {
        console.log($(e.currentTarget).attr('data-dismiss'));

        var phone_number = $('#phone_numberActivate').val()
        console.log(phone_number);
        var user_id = {{Auth::user()->id}};
        console.log(user_id);


        $.ajax({
            url: url + '/hq-admin/subscription/activate',
            method: 'POST',
            data: {
                "_token": "{{ csrf_token() }}",
                "phone_number": phone_number
            },
            success: function (data) {
                console.log(data);
                toastr.success('Subscription Added');
                window.location.href = url + "/hq-admin/subscription/list";
            },
            error: function (err) {
                console.log(err);

                if (err.custom) {
                    toastr.error("An Error occurred. Failed to add subscription. " + err.custom)
                } else {
                    toastr.error("An Error occurred. Failed to add subscription");
                }
            }
        })
        ID = $(e.relatedTarget).attr('data-dismiss');
        console.log(ID);
    });

    /*hide / show phone number*/
    $('#otherNumberActivate').change(function () {
        if (this.checked) {
            console.log("checked");
            // the checkbox is now checked
        } else {
            // the checkbox is now no longer checked
        }
        console.log("click me");
        $('#phone_numberActivate').toggleClass('hidden');
    });

    /*hide / show phone number*/
    $('#otherNumberSubscribe').change(function () {
        if (this.checked) {
            console.log("checked");
            // the checkbox is now checked
        } else {
            // the checkbox is now no longer checked
        }
        console.log("click me");
        $('#phone_numberSubscribe').toggleClass('hidden');
    });
</script>

@endsection