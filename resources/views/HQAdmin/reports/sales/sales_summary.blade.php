@extends('layouts.admin')
@section('content')
    <div class="modal  fade" tabindex="-1" role="dialog" id="delete">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Delete Employee</h4>
                </div>
                {!! Form::open(['url'=>'delete/user','method'=>'GET']) !!}
                <div class="modal-body">
                    <input hidden id="del" name="id">
                    <p>Are you sure you want to delete this employee &hellip;</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-danger">Delete</button>
                </div>
                {!! Form::close() !!}
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content">


                    <div class="well" style="overflow: auto">
                        <div class="col-xs-4 col-md-4 col-lg-4">
                            <div id="reportrange_right" class="pull-left"
                                 style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                <span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
                            </div>
                        </div>
                        {!! Form::open(['hq-admin/report/sales/summary' => '', 'method' => 'get'])!!}
                        <input type="hidden" name="start_date" id="start_date">
                        <input type="hidden" name="end_date" id="end_date">
                        {{-- clients Form Input--}}
                        <div class="col-xs-4 col-md-4 col-lg-4">
                            {!! Form::select('client',$shops,null,(['class'=>'form-control','id'=>'client','style'=>'width:100%','name'=>'shop']))!!}
                        </div>
                        {{--<div class="button-group">
                            <div class="">
                                {!! Form::submit('Generate Report', ['class' => 'btn btn-primary btn-sm pull-right']) !!}
                            </div>
                            <button type="button" class="btn btn-primary">Primary</button>
                        </div>--}}
                        <div class="col-xs-4 col-md-4 col-lg-4">
                            <a href="{{url('hq-admin/report/sales/summary')}}"
                               class="btn btn-success pull-right btn-large">Reset</a>
                            <button class="btn btn-primary pull-right" type="submit">Generate Report</button>
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <h3>{{$title}}</h3>
            <div class="x_panel">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                        <span>Net Shop Sales</span>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="x_content">
                        @if(count($chart_data))
                            <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                        @else()
                            <div class="table-cell vertical-middle height-100">
                                <div class=" text-center" style="height: 25rem">
                                    <img src="{{url('/assets/images/icon_report_flat.png')}}"
                                         style="margin-bottom: 15px; " alt="">
                                    <div translate="NO_DATA_TO_DISPLAY" class="text-center">No data to display
                                    </div>
                                </div>
                            </div>
                            {{--
                                                                <div style="min-width: 310px; height: 25rem; margin: 0 auto"></div>
                            --}}
                        @endif()
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($type == 'byItem')
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>List Sales
                            <small></small>
                        </h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        {!! $dataTable->table(['class' => 'table table-striped responsive','style'=>"width:100%"]) !!}
                    </div>
                </div>
            </div>
        </div>
    @else()
    @endif()
@endsection


@section('scripts')

    <script>
        $(function () {
            $("#delete").on('show.bs.modal', function (e) {
                ID = $(e.relatedTarget).attr('id');
                $("#del").val(ID);
            })
        });


    </script>
    @if($type == 'byItem')
        {!! $dataTable->scripts() !!}
    @endif()



    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script>
        var data = <?php echo json_encode($chart_data); ?>;
        var type = <?php echo json_encode($type);?>;
        console.log(type);
        $('document').ready(function () {
            if (data.length) {
                if (type == 'summary') {
                    var chart_data = [];
                    var categories = [];

                    $.each(data, function (key, value) {
                        var my_array = [];
                        $.each(value.data, function (k, v) {
                            var stupid = moment(v[0]);
                            var year = stupid.get("year");
                            var month = stupid.get("month");
                            var day = stupid.get("date");
                            var dates = [
                                Date.UTC(year, month, day), parseFloat(v[1])
                            ];
                            my_array.push(dates)
                            return;
                        });
                        var obj = {
                            name: value.name,
                            data: my_array
                        }
                        chart_data.push(obj);
                    });

                    drawLineChart(chart_data);
                } else if (type == 'byItem') {
                    console.log('byItem');
                    /*format the data*/
                    var x_axis = [];
                    var y_axis = [];
                    var product_names = [];
                    var series_data = [];

                    $.each(data, function (key, value) {
                        if (value.date != '0000-00-00') {
                            var date = moment(value.sale_date).format('D/M/Y');
                            x_axis.push(date);
                            product_names.push(value.product_name);
                        }
                        column = {};
                    });
                    /*get unique x_axis values*/
                    x_axis = Array.from(new Set(x_axis));
                    console.log(x_axis);

                    /*get unique product names*/
                    var unique = Array.from(new Set(product_names));

                    /*loop through the unique product names and build the chart data array*/
                    $.each(unique, function (p_key, p_value) {
                        var product_data = [];
                        /*loop through x axis date values*/
                        $.each(x_axis, function (x_key, x_value) {
                            /*loop through the server products data*/
                            var current_total = 0;

                            $.each(data, function (sp_key, sp_value) {
                                if (sp_value.product_name == p_value && (moment(sp_value.sale_date).format('D/M/Y') == x_value)) {
                                    current_total += sp_value.total_amount;
                                }
                            });
                            product_data.push(current_total);
                        });
                        /*single product*/
                        var single_prod = {
                            'name': p_value,
                            'data': product_data
                        }
                        /*push single prod to series_data*/
                        series_data.push(single_prod);

                    });
                    console.log(series_data);
                    drawChart(x_axis, series_data);
                }
            }
        });


        function drawLineChart(chart_data) {
            Highcharts.chart('container', {
                chart: {
                    type: 'spline'
                },
                title: {
                    text: 'Shop Sales'
                },
                xAxis: {
                    type: 'datetime',
                    dateTimeLabelFormats: { // don't display the dummy year
                        month: '%e. %b',
                        year: '%Y'
                    },
                    title: {
                        text: 'Date'
                    }
                },
                yAxis: {
                    title: {
                        text: 'Sales (Ksh.)'
                    },
                    min: 0
                },
                tooltip: {
                    headerFormat: '<b>{series.name}</b><br>',
                    pointFormat: '{point.x:%e. %b %Y}: {point.y:.2f} m'
                },

                plotOptions: {
                    spline: {
                        marker: {
                            enabled: true
                        }
                    }
                },
                series: chart_data
            });
        }

        function drawChart(x_axis, series_data) {
            Highcharts.chart('container', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Sales Per Product'
                },
                xAxis: {
                    categories: x_axis
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Amount'
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                legend: {
                    align: 'right',
                    x: -30,
                    verticalAlign: 'top',
                    y: 25,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false
                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                        }
                    }
                },
                series: series_data
            });
            $('.highcharts-credits').attr('class', 'hide');
        }
    </script>
    <script>
        $('document').ready(function () {

            var start_date = <?php echo json_encode($start_date)?>;
            var end_date =<?php echo json_encode($end_date)?>;

            $('#start_date').val(moment(start_date.date).format('Y-M-D'));
            $('#end_date').val(moment(end_date.date).format('Y-M-D'));
            $('#reportrange_right span').html(moment(start_date.date).format('MMMM D, YYYY') + ' - ' + moment(end_date.date).format('MMMM D, YYYY'));

            $('#client').select2({
                ajax: {
                    url: url + '/hq-admin/shop_search',
                    delay: 250,
                    data: function (params) {
                        var query = {
                            search: params.term,
                            page: params.page || 1
                        }
                        // Query parameters will be ?search=[term]&page=[page]
                        return query;
                    },
                    cache: true
                },
                placeholder: 'Select Shop',
            });
        })

        $('#reportrange_right').on('apply.daterangepicker', function (ev, picker) {
            $('#start_date').val(picker.startDate.format('Y-M-D'));
            $('#end_date').val(picker.endDate.format('Y-M-D'));
        });
    </script>

@endsection