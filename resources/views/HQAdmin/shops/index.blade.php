@extends('layouts.admin')
@section('content')
<div class="modal  fade" tabindex="-1" role="dialog" id="delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Delete Employee</h4>
            </div>
            {!! Form::open(['url'=>'delete/user','method'=>'GET']) !!}
            <div class="modal-body">
                <input hidden id="del" name="id">
                <p>Are you sure you want to delete this employee &hellip;</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-danger">Delete</button>
            </div>
            {!! Form::close() !!}
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>List Shops
                    <small></small>
                </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                {!! $dataTable->table(['class' => 'table table-striped responsive','style'=>"width:100%"]) !!}
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')

    <script>
        $(function () {
            $("#delete").on('show.bs.modal', function (e) {
                ID = $(e.relatedTarget).attr('id');
                $("#del").val(ID);
            })
        });


    </script>
    {!! $dataTable->scripts() !!}

@endsection