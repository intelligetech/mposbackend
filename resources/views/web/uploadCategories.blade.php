@extends('layouts.admin')

@section('content')

    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-8">
                {!! Form::open(['url'=>'admin/categories/upload/save','files'=>true ,'method'=>'POST',"enctype"=>"multipart/form-data" ]) !!}
                {!! Form::label('Select A Shop To upload Category Details:') !!}
                {!! Form::select('shop_id',$shops,null,['class'=>'form-control','id'=>'shop']) !!}<br>
                {!! Form::label('Select A Department To upload category Details:') !!}
                {!! Form::select('department_id',$none,null,['class'=>'form-control','id'=>'depts']) !!}
                <br>
                {!! Form::label('Select An Excel File With Category Details:') !!}
                {!! Form::file('file',['class'=>'form-control']) !!} <br>
                {!! Form::submit('upload',['class'=>'btn btn-primary btn-block']) !!}
                {!! Form::close() !!}

            </div>
        </div>
    </div>
@section('scripts')
    <script type="text/javascript">


    </script>
    <script>
        $(function () {

            var data = {!!json_encode($depts)!!};

            console.log(data);

            var selectedShop = $("select#shop").val();
            if (data[selectedShop].length == 0) {
                window.alert("No departments for this shop please upload departments first");
            } else {
                var sel_dept = data[selectedShop];
                $.each(sel_dept, function (key, value) {
                    $('#depts').append("<option value='" + key + "'>" + value + "</option");
                });
            }
            $("#shop").on('change', function (e) {
                $('#depts').empty();
                var selectedShop = $("select#shop").val();
                console.log(selectedShop);
                if (data[selectedShop].length == 0) {
                    window.alert("No departments for this shop please upload departments first");
                } else {
                    var sel_dept = data[selectedShop];
                    $.each(sel_dept, function (key, value) {
                        $('#depts').append("<option value='" + key + "'>" + value + "</option");
                    });
                }

            })
        });
    </script>
@endsection
@stop