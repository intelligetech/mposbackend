@extends('layouts.admin')

@section('content')

    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-8">
                {!! Form::open(['url'=>'admin/products/upload/save','files'=>true ,'method'=>'POST',"enctype"=>"multipart/form-data" ]) !!}
                {!! Form::label('Select A Shop To upload Products:') !!}
                {!! Form::select('shop_id',$shops,null,['class'=>'form-control','id'=>'shop']) !!}<br>
                {!! Form::label('Select A Category for the Products:') !!}
                {!! Form::select('category_id',$none,null,['class'=>'form-control','id'=>'categories']) !!}
                <br>
                {!! Form::label('Select An Excel File With Products:') !!}
                {!! Form::file('file',['class'=>'form-control']) !!} <br>
                {!! Form::submit('upload',['class'=>'btn btn-primary btn-block']) !!}
                {!! Form::close() !!}

            </div>
        </div>
    </div>
@section('scripts')
    <script type="text/javascript">


    </script>
    <script>
        $(function () {

            var data = {!!json_encode($categories)!!};

            console.log(data);

            var selectedShop = $("select#shop").val();
            if (data[selectedShop].length == 0) {
                window.alert("No categories for this shop please upload categories first");
            } else {
                var sel_dept = data[selectedShop];
                $.each(sel_dept, function (key, value) {
                    $('#categories').append("<option value='" + key + "'>" + value + "</option");
                });
            }
            $("#shop").on('change', function (e) {
                $('#categories').empty();
                var selectedShop = $("select#shop").val();
                console.log(selectedShop);
                if (data[selectedShop].length == 0) {
                    window.alert("No categories for this shop please upload categories first");
                } else {
                    var sel_dept = data[selectedShop];
                    $.each(sel_dept, function (key, value) {
                        $('#categories').append("<option value='" + key + "'>" + value + "</option");
                    });
                }

            })
        });
    </script>
@endsection
@stop