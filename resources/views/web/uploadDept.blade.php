@extends('layouts.admin')

@section('content')

    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-8">
                {!! Form::open(['url'=>'admin/department/upload/save','files'=>true ,'method'=>'POST',"enctype"=>"multipart/form-data" ]) !!}
                {!! Form::label('Select A Shop To upload Department Details:') !!}
                {!! Form::select('shop_id',$shops,null,['class'=>'form-control']) !!}<br>
                {!! Form::label('Select An Excel File With New Department Details:') !!}
                {!! Form::file('file',['class'=>'form-control']) !!} <br>
                {!! Form::submit('upload',['class'=>'btn btn-primary btn-block']) !!}
                {!! Form::close() !!}

            </div>
        </div>

    </div>

@stop