@extends('layouts.admin')
@section('content')
    <div class="modal  fade" tabindex="-1" role="dialog" id="delete">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Delete Employee</h4>
                </div>
                {!! Form::open(['url'=>'delete/user','method'=>'GET']) !!}
                <div class="modal-body">
                    <input hidden id="del" name="id">
                    <p>Are you sure you want to delete this employee &hellip;</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-danger">Delete</button>
                </div>
                {!! Form::close() !!}
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content">


                    <div class="well" style="overflow: auto">
                        <div class="col-xs-4 col-md-4 col-lg-4">
                            <div id="reportrange_right" class="pull-left"
                                 style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                <span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
                            </div>
                        </div>
                        {!! Form::open(['super-admin/report/sales/summary' => '', 'method' => 'get'])!!}
                        <input type="hidden" name="start_date" id="start_date">
                        <input type="hidden" name="end_date" id="end_date">
                        {{-- clients Form Input--}}
                        <div class="col-xs-4 col-md-4 col-lg-4">
                            {!! Form::select('client',$shops,null,(['class'=>'form-control','id'=>'client','style'=>'width:100%','name'=>'shop']))!!}
                        </div>
                        <div class="col-xs-4 col-md-4 col-lg-4">
                            {!! Form::submit('Generate Report', ['class' => 'btn btn-primary btn-sm pull-right']) !!}
                        </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <h3>{{$title}}</h3>
            <div class="x_panel">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="row top_tiles">
                            <div class="animated flipInY">
                                <div class="tile-stats">
                                    <div class="icon"><i class="fa fa-money"></i></div>
                                    <div class="count">{{$total_sales}}</div>
                                    <h3>Total Sales</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-lg-6 text-center">
                                <span>
                                    Net Sales For Period
                                </span>
                            </div>
                            <div class="col-md-6 col-sm-6 col-lg-6">

                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="x_content">
                                @if(count($chart_data))
                                    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                                @else()
                                    <div class="table-cell vertical-middle height-100">
                                        <div class=" text-center" style="height: 25rem">
                                            <img src="{{url('/assets/images/icon_report_flat.png')}}"
                                                 style="margin-bottom: 15px; " alt="">
                                            <div translate="NO_DATA_TO_DISPLAY" class="text-center">No data to display
                                            </div>
                                        </div>
                                    </div>
                                    {{--
                                                                        <div style="min-width: 310px; height: 25rem; margin: 0 auto"></div>
                                    --}}
                                @endif()
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>List Sales
                        <small></small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    {!! $dataTable->table(['class' => 'table table-striped responsive','style'=>"width:100%"]) !!}
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')

    <script>
        $(function () {
            $("#delete").on('show.bs.modal', function (e) {
                ID = $(e.relatedTarget).attr('id');
                $("#del").val(ID);
            })
        });


    </script>
    {!! $dataTable->scripts() !!}


    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script>
        var data = <?php echo json_encode($chart_data); ?>;
        $('document').ready(function () {
            if (data.length) {
                drawChart();
            }
        });

        var x_axis = [];
        var y_axis = [];
        $.each(data, function (key, value) {
            if (value.date != '0000-00-00') {
                var date = moment(value.date).format('D/M/Y');
                x_axis.push(date);
                y_axis.push(value.amount);
            }
        });

        function drawChart() {
            Highcharts.chart('container', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: ''
                },
                xAxis: {
                    categories: x_axis,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'amount (Ksh.)'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:,.2f} Ksh.</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Period',
                    data: y_axis
                }],
            });
            $('.highcharts-credits').attr('class', 'hide');
        }


    </script>
    <script>
        $('document').ready(function () {

            var start_date = <?php echo json_encode($start_date)?>;
            var end_date =<?php echo json_encode($end_date)?>;

            $('#start_date').val(moment(start_date.date).format('Y-M-D'));
            $('#end_date').val(moment(end_date.date).format('Y-M-D'));
            $('#reportrange_right span').html(moment(start_date.date).format('MMMM D, YYYY') + ' - ' + moment(end_date.date).format('MMMM D, YYYY'));

            $('#client').select2({
                ajax: {
                    url: url + '/super-admin/shop_search',
                    delay: 250,
                    data: function (params) {
                        var query = {
                            search: params.term,
                            page: params.page || 1
                        }
                        // Query parameters will be ?search=[term]&page=[page]
                        return query;
                    },
                    cache: true
                },
                placeholder: 'Select Shop',
            });
        })

        $('#reportrange_right').on('apply.daterangepicker', function (ev, picker) {
            $('#start_date').val(picker.startDate.format('Y-M-D'));
            $('#end_date').val(picker.endDate.format('Y-M-D'));
        });
    </script>

@endsection