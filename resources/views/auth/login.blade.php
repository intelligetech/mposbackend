@extends('layouts.wrapper')

@section('content')

    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
                <form class="form-horizontal" role="form" method="POST" action="{{route('login')}}">
                    {{ csrf_field() }}

                    <h1>Login Form</h1>
                    <div>
                        <input id="username" type="text" class="form-control" name="username"
                               value="{{ old('username')}}" required autofocus>
                        @if ($errors->has('username'))
                            <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div>
                        <input id="password" type="password" class="form-control" onchange="myFunction()" required>
                        <input type="hidden" id="pass" name="password">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div>
                        <button type="submit" class="btn btn-primary">
                            Login
                        </button>
                    </div>

                    <div class="clearfix"></div>

                    <div class="separator">
                        <div class="clearfix"></div>
                        <br/>

                        <div>
                            <h1><i class="fa fa-paw"></i> Lyte Shop WEB APP</h1>
                            <p>©2017 All Rights Reserved. Lyte Shop WEB PORTAL!</p>
                        </div>
                    </div>
                </form>
            </section>
        </div>

    </div>


@endsection

@section('scripts')
    <script>
        function myFunction() {
            var x = document.getElementById("password").value;
            var y = CryptoJS.SHA256(x).toString(CryptoJS.enc.Base64);
            $("#pass").val(y);
            console.log(y);
        }


    </script>

@endsection
