<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Lyte Shop WEB APP | </title>

    <!-- Bootstrap -->
    <link href="{{url('vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{url('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{url('vendors/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="{{url('vendors/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{url('build/css/custom.min.css')}}" rel="stylesheet">
    {{--datatables--}}
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    {{--mystyle.css--}}
    <link href="{{url('assets/css/mystyle.css')}}" rel="stylesheet">
    {{--select 2 css--}}
    <link href="{{url('vendors/select2/dist/css/select2.min.css')}}" rel="stylesheet"/>

    {{--Toastr--}}
    <link href="{{url('js/toastr/toastr.min.css')}}" rel="stylesheet">


</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="" class="site_title"><i class="fa fa-paw"></i> <span>Lyte Shop WEB APP</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <img src="{{url('images/user.png')}}" alt="..."
                             class="img-circle
                         profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Welcome, {{Auth::user()->username}}</span>
                        @if(Auth::user()->role_id == 1)
                            <p>HQ Admin</p>
                            <p>{{$hq->name}}</p>
                            <h2></h2>
                        @else()
                            <p>Super Admin</p>
                        @endif()
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br/>

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>General</h3>
                        @if(Auth::user()->role_id == 1)
                            {{--hq admin--}}
                            <ul class="nav side-menu">
                                <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{url('/home')}}">Dashboard</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a><i class="fa fa-user"></i>Users<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{url('list/users')}}">List Users</a></li>
                                        <li><a href="{{url('list/suppliers')}}">List Suppliers</a></li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-building"></i>Shops<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{url('list/shops')}}">List Shops</a></li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-shopping-basket"></i>Inventory<span
                                                class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{url('list/departments')}}">List Departments</a></li>
                                        <li><a href="{{url('upload/departments')}}">Upload Departments</a></li>
                                        <li><a href="{{url('list/categories')}}">List Categories</a></li>
                                        <li><a href="{{url('upload/categories')}}">Upload Categories</a></li>
                                        <li><a href="{{url('list/products')}}">List Products</a></li>
                                        <li><a href="{{url('upload/products')}}">Upload Products</a></li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-linux"></i>Settings<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{url('list/roles')}}">List Roles</a></li>
                                        <li><a href="{{url('list/tax')}}">List Tax</a></li>
                                        <li><a href="{{url('hq-admin/subscription/list')}}">Subscription</a>

                                    </ul>
                                </li>
                                <li><a><i class="fa fa-shopping-cart"></i>Orders<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{url('list/orders')}}">List Orders</a></li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-line-chart"></i> Reports <span
                                                class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a>Sales<span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu">
                                                <li class="sub_menu"><a
                                                            href="{{url('hq-admin/report/sales/summary')}}">Sales
                                                        Summary</a>
                                                </li>
                                                {{--
                                                <li><a href="#level2_2">Level Two</a>
                                                </li>--}}
                                            </ul>
                                        </li>{{--
                                        <li><a href="#level1_2">Level One</a>
                                        </li>--}}
                                    </ul>
                                </li>
                            </ul>
                        @else()
                            {{--super admin--}}
                            <ul class="nav side-menu">
                                <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{url('/home')}}">Dashboard</a></li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-linux"></i>Settings<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{url('super-admin/status/list')}}">List Status</a></li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-linux"></i>Subscriptions<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{url('super-admin/subscription/list')}}">List Subscriptions</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a><i class="fa fa-user"></i>Users<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{url('super-admin/list/users')}}">List Users</a></li>
                                        <li><a href="{{url('super-admin/list/suppliers')}}">List Suppliers</a></li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-building"></i>Shops<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{url('super-admin/list/shops')}}">List Shops</a></li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-line-chart"></i> Reports <span
                                                class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a>Sales<span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu">
                                                <li class="sub_menu"><a
                                                            href="{{url('super-admin/report/sales/summary')}}">Sales
                                                        Summary</a>
                                                </li>
                                                <li><a href="{{url('super-admin/report/sales/byItem')}}">Sales
                                                        By Item</a>
                                                </li>{{--
                                                <li><a href="#level2_2">Level Two</a>
                                                </li>--}}
                                            </ul>
                                        </li>{{--
                                        <li><a href="#level1_2">Level One</a>
                                        </li>--}}
                                    </ul>
                                </li>
                            </ul>
                        @endif()
                    </div>

                </div>
                <!-- /sidebar menu -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                               aria-expanded="false">
                                <img src="{{url('images/user.png')}}" alt="">
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="{{url('logout')}}"><i class="fa fa-sign-out pull-right"></i> Log
                                        Out</a></li>
                            </ul>
                        </li>

                        <li role="presentation" class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown"
                               aria-expanded="false">
                                <i class="fa fa-envelope-o"></i>
                                <span class="badge bg-green">6</span>
                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                <li>
                                    <a>
                                        <span class="image"><img src="{{url('images/user.png')}}" alt="Profile Image"/></span>
                                        <span>
                                            <span>John Smith</span>
                                            <span class="time">3 mins ago</span>
                                        </span>
                                        <span class="message">
                                            Film festivals used to be do-or-die moments for movie makers. They were where...
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="{{url('images/img.jpg')}}"
                                                                 alt="Profile Image"/></span>
                                        <span>
                                            <span>John Smith</span>
                                            <span class="time">3 mins ago</span>
                                        </span>
                                        <span class="message">
                                            Film festivals used to be do-or-die moments for movie makers. They were where...
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="{{url('images/img.jpg')}}"
                                                                 alt="Profile Image"/></span>
                                        <span>
                                            <span>John Smith</span>
                                            <span class="time">3 mins ago</span>
                                        </span>
                                        <span class="message">
                                            Film festivals used to be do-or-die moments for movie makers. They were where...
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image"><img src="{{url('images/img.jpg')}}"
                                                                 alt="Profile Image"/></span>
                                        <span>
                                            <span>John Smith</span>
                                            <span class="time">3 mins ago</span>
                                        </span>
                                        <span class="message">
                                            Film festivals used to be do-or-die moments for movie makers. They were where...
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <div class="text-center">
                                        <a>
                                            <strong>See All Alerts</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                @include('notices.success')
                @include('notices.fail')
                @yield('content')

            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">
                Lyte Shop WEB APP created by <a href="">Intelligent Technologies</a>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script> var url = "{{ url('') }}"; </script>
<script src="{{url('vendors/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{url('vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{url('vendors/fastclick/lib/fastclick.js')}}"></script>
<!-- NProgress -->
<script src="{{url('vendors/nprogress/nprogress.js')}}"></script>
<!-- Chart.js -->
<script src="{{url('vendors/Chart.js/dist/Chart.min.js')}}"></script>
<!-- jQuery Sparklines -->
<script src="{{url('vendors/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<!-- Flot -->
<script src="{{url('vendors/Flot/jquery.flot.js')}}"></script>
<script src="{{url('vendors/Flot/jquery.flot.pie.js')}}"></script>
<script src="{{url('vendors/Flot/jquery.flot.time.js')}}"></script>
<script src="{{url('vendors/Flot/jquery.flot.stack.js')}}"></script>
<script src="{{url('vendors/Flot/jquery.flot.resize.js')}}"></script>
<!-- Flot plugins -->
<script src="{{url('vendors/flot.orderbars/js/jquery.flot.orderBars.js')}}"></script>
<script src="{{url('vendors/flot-spline/js/jquery.flot.spline.min.js')}}"></script>
<script src="{{url('vendors/flot.curvedlines/curvedLines.js')}}"></script>
<!-- DateJS -->
<script src="{{url('vendors/DateJS/build/date.js')}}"></script>
<!-- bootstrap-daterangepicker -->
<script src="{{url('vendors/moment/min/moment.min.js')}}"></script>
<script src="{{url('vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>

<!-- Custom Theme Scripts -->
<script src="{{url('build/js/custom.js')}}"></script>
<script>
    //slide up the notification
    $(".notify").delay(3000).slideUp('slow');
</script>
{{--datatables--}}
<link rel="stylesheet" href="{{url('/vendor/datatables/Buttons/css/buttons.bootstrap.min.css')}}">
<script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script src='{{url("/vendor/datatables/buttons.server-side.js")}}'></script>

{{--Moment--}}
{{--
<script src="{{url('js/moment/moment.min.js')}}"></script>
--}}

{{--select2 js--}}
<script src="{{url('vendors/select2/dist/js/select2.min.js')}}"></script>
<script src="{{url('js/toastr/toastr.min.js')}}"></script>

@yield('scripts')
</body>
</html>