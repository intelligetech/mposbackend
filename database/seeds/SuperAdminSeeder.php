<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->username = 'superadmin';
        $user->email = 'superadmin@gmail.com';
        $user->password = 'XohImNooBHFR0OVvjcYpJ3NgPQ1qq73WKhHvch0VQtg=';
        $user->phone_number = '0724954409';
        $user->role_id = 1000;
        $user->created_at = new Carbon();
        $user->updated_at = new Carbon();
        $user->save();

    }
}
