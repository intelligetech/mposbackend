<?php

use App\Role;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->id = 1000;
        $role->role_name = 'Super Admin';
        $role->description = 'Super Admin Role';
        $role->created_at = new Carbon();
        $role->updated_at = new Carbon();
        $role->save();
    }
}
