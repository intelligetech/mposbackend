<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptionPayments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('local_id');
            $table->integer('HQID')->unsigned();
            $table->foreign('HQID')->references('id')->on('headquarter');
            $table->integer('user_id');
            $table->unique(array('local_id', 'HQID'));
            $table->integer('sync');
            $table->integer('sync_id')->unsigned();
            $table->foreign('sync_id')
                ->references('id')
                ->on('sync');
            $table->string('expiry_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptionPayments');
    }
}
