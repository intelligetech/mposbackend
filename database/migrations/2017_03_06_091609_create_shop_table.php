<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('local_id');
            //$table->integer('id')->primary();
            $table->string('shop_name', 50);
            $table->string('address', 50);
            $table->string('phone_number', 25);
            $table->string('pin_number', 25);
            $table->string('vat', 25)->nullable();
            $table->integer('HQID')->unsigned();
            $table->foreign('HQID')->references('id')->on('headquarter');
            $table->integer('sync');
            $table->integer('user_id');
            $table->integer('sync_id')->unsigned();
            $table->foreign('sync_id')->references('id')->on('sync');
            $table->unique(array('local_id', 'HQID', 'user_id'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop');
    }
}
