<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderSupplyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_supply', function (Blueprint $table) {
            //$table->integer('id')->primary();
            $table->increments('id');
            $table->integer('local_id');
            $table->string('code', 50);
            $table->string('ship_to', 200);
            $table->string('expected_date',50);
            $table->integer('total_amount')->nullable();
            $table->integer('shop_id');
            $table->integer('supplier_id');
            $table->integer('flag');
            $table->integer('user_id');
            $table->integer('HQID')->unsigned();
            $table->foreign('HQID')->references('id')->on('headquarter');
            $table->unique(array('local_id', 'HQID', 'user_id'));
            $table->string('status', 50)->nullable();
            $table->integer('sync');
            $table->integer('sync_id')->unsigned();
            $table->integer('shopID');
            $table->foreign('sync_id')
                ->references('id')
                ->on('sync');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_supply');
    }
}
