<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStkpushTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stkpush', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hq_id')->unsigned()->nullable();
            $table->integer('subscription_id')->unsigned()->nullable();
            $table->string('merchantRequestID', 255)->nullable();
            $table->string('checkoutRequestID', 255)->nullable();
            $table->integer('responseCode')->nullable();
            $table->text('responseDescription')->nullable();
            $table->text('customerMessage')->nullable();
            $table->integer('resultCode')->nullable();
            $table->longText('resultDescription')->nullable();
            $table->longText('callBackMetaData')->nullable();
            $table->foreign('subscription_id')
                ->references('id')
                ->on('subscriptions')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stkpush');
    }
}
