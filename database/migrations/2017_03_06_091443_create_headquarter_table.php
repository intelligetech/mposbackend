<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeadquarterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('headquarter', function (Blueprint $table) {
            //$table->integer('id')->primary();
            $table->increments('id');
            $table->string('name',50);
            $table->string('location',50);
            $table->string('description',200)->nullable();
            $table->integer('sync')->default(0);;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('headquarter');
    }
}
