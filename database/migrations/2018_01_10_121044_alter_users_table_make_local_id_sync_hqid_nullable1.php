<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTableMakeLocalIdSyncHqidNullable1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('local_id')->nullable()->change();
            $table->dropForeign(['HQID']);
            $table->integer('HQID')->nullable()->unsigned()->change();
            $table->foreign('HQID')->references('id')->on('headquarter');
            $table->dropForeign(['sync_id']);
            $table->integer('sync_id')->nullable()->unsigned()->change();
            $table->foreign('sync_id')->references('id')->on('sync');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('local_id')->change();
            $table->dropForeign(['sync_id']);
            $table->integer('sync')->unsigned()->change();
            $table->foreign('sync_id')->references('id')->on('sync');
            $table->dropForeign(['HQID']); // Drop foreign key 'user_id' from 'posts' table
            $table->integer('HQID')->unsigned()->change();
            $table->foreign('HQID')->references('id')->on('headquarter');
        });
    }
}
