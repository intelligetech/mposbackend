<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            //$table->integer('id')->primary();
            $table->increments('id');
            $table->integer('local_id');
            $table->integer('product_id');
            $table->integer('order_supply_id');
            $table->integer('quantity_ordered');
            $table->integer('price_per_item');
            $table->integer('flag');
            $table->integer('sync');
            $table->timestamps();
            $table->integer('user_id');
            $table->integer('HQID')->unsigned();
            $table->foreign('HQID')->references('id')->on('headquarter');
            $table->unique(array('local_id', 'HQID', 'user_id'));
            $table->integer('sync_id')->unsigned();
            $table->integer('shopID');
            $table->foreign('sync_id')
                ->references('id')
                ->on('sync');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
