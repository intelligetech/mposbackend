<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill_products', function (Blueprint $table) {
            //$table->integer('id')->primary();
            $table->increments('id');
            $table->integer('local_id');
            $table->integer('product_id');
            $table->integer('qty');
            $table->double('price');
            $table->double('discount')->nullable();
            $table->double('tax')->nullable();
            $table->integer('bill_id');
            $table->string('created_time')->nullable();
            $table->integer('user_id');
            $table->integer('HQID')->unsigned();
            $table->foreign('HQID')->references('id')->on('headquarter');
            $table->unique(array('local_id', 'HQID', 'user_id'));
            $table->integer('sync');
            $table->integer('sync_id')->unsigned();
            $table->integer('shopID');
            $table->foreign('sync_id')
                ->references('id')
                ->on('sync');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bill_products');
    }
}
