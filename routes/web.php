<?php
if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
    // Ignores notices and reports all other kinds... and warnings
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
    // error_reporting(E_ALL ^ E_WARNING); // Maybe this is enough
}
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('logout', 'WebController@logout');


Route::group(['middleware' => 'cors', 'prefix' => 'api'], function () {
    Route::resource('authenticate', 'MobileAppController', ['only' => ['index']]);
    Route::post('authenticate', 'MobileAppController@authenticate');
    Route::get('authenticate/user', 'MobileAppController@getAuthenticatedUser');
    Route::get('/sync/add_table', 'MobileAppController@add_table');
    Route::post('/sync/insert_data', 'MobileAppController@insert_data');
    Route::post('/registerHq', 'MobileAppController@registerHq');
    Route::post('/sync/getsyncID', 'MobileAppController@getsyncID');
    Route::post('/adminLogin', 'MobileAppController@adminLogin');
    Route::post('/downloadSync', 'MobileAppController@downloadSync');
    Route::post('/checkUsername', 'MobileAppController@checkUsername');


    /*subscription*/
    Route::get('/getSubscription/{hqid}', 'MobileControllers\SubscriptionController@checkSubscription');
    Route::post('/subscription/activate', 'MobileAppController@save');
    Route::post('/subscription/checkActivation', 'MobileAppController@checkActivation');


});


Auth::routes();

Route::get('/home', 'HomeController@index');


//Routes for the web portal
Route::get('list/users', 'WebController@users');
Route::get('list/suppliers', 'WebController@suppliers');
Route::get('list/shops', 'WebController@shops');
Route::get('list/departments', 'WebController@departments');
Route::get('upload/departments', 'WebController@uploadDept');
Route::post('admin/department/upload/save', 'WebController@saveDept');
Route::get('list/categories', 'WebController@categories');
Route::get('upload/categories', 'WebController@uploadCategories');
Route::post('admin/categories/upload/save', 'WebController@saveCategories');
Route::get('list/products', 'WebController@products');
Route::get('upload/products', 'WebController@uploadProducts');
Route::post('admin/products/upload/save', 'WebController@saveProducts');
Route::get('list/roles', 'WebController@roles');
Route::get('list/tax', 'WebController@tax');
Route::get('list/orders', 'WebController@orders');
Route::post('customLogin', 'LoginController@postLogin');
Route::get('list/sales/report', 'WebController@salesReport');
Route::post('reports/saveSale', 'WebController@generateSale');

/*Super Admin*/
Route::group(['middleware' => 'super-admin', 'prefix' => 'super-admin'], function () {
    /*settings*/
    /*status*/
    Route::get('status/list', 'SuperAdminControllers\StatusController@index');
    Route::get('status/add', 'SuperAdminControllers\StatusController@add');
    Route::post('status/save', 'SuperAdminControllers\StatusController@save');


    /*subscriptions*/
    Route::get('subscription/list', 'SuperAdminControllers\SubscriptionController@index');

    /*users*/
    Route::get('list/users', 'UsersController@index');

    /*suppliers*/
    Route::get('list/suppliers', 'SuppliersController@index');
    Route::get('list/shops', 'ShopsController@index');

    /*Reports*/
    /*sales report*/
    Route::get('report/sales/summary', [
        'as' => 'reports.sales_summary',
        'uses' => 'SalesReportController@summary']);
    Route::get('report/sales/byItem', 'SalesReportController@salesByItem');

    /*shop search*/
    Route::get('shop_search', 'ShopSearchController@shopSearch');

});

/*HQ Admin*/
Route::group(['middleware' => 'hq-admin', 'prefix' => 'hq-admin'], function () {
    /*Sales Report*/
    Route::get('report/sales/summary', 'AdminSalesReportController@summary');
    /*shop search*/
    Route::get('shop_search', 'ShopSearchController@adminShopSearch');

    /*subscriptions*/
    Route::get('subscription/list', 'HQAdminControllers\SubscriptionController@index');
    Route::post('subscription/add', 'HQAdminControllers\SubscriptionController@save');
    Route::post('subscription/check', 'HQAdminControllers\SubscriptionController@checkActivation');
    Route::post('subscription/activate', 'HQAdminControllers\SubscriptionController@activate');

});

Route::post('mpesaConfirm', 'MpesaControllers\MpesaController@mpesaConfirm');
