<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class departments extends Model
{
    protected $guarded = [];
    protected $table = 'departments';

}
