<?php

namespace App\Http\Controllers;

use App\DataTables\SalesByItemReportDataTable;
use App\DataTables\SalesReportDataTable;
use App\DataTables\SuppliersDataTable;
use App\payment;
use App\shop;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SalesReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function summary(SalesReportDataTable $dataTable, Request $request)
    {
        $query = $dataTable->query();
        $start_date = Carbon::create()->subMonth();
        $end_date = Carbon::create();

        $sum_query = payment::where('payment.time', '<>', 0);


        if ($request->has('start_date') && $request->get('start_date') && $request->has('end_date') && $request->get('end_date')) {
            $start_date = new Carbon($request->get('start_date'));
            $end_date = new Carbon($request->get('end_date'));
        }

        $sum_query->where('payment.time', '>=', $start_date);
        $sum_query->where('payment.time', '<=', $end_date);

        $shop_name = 'All Shops';
        if ($request->has('shop') && $request->get('shop')) {
            $shop_id = $request->get('shop');
            $shop = shop::find($shop_id);

            $hq_id = $shop->HQID;
            $local_id = $shop->local_id;

            $sum_query->where('payment.HQID', $hq_id)
                ->where('payment.shopID', $local_id);
            $shop_name = $shop->shop_name;
        }
        $displayStartDate = new Carbon($start_date);
        $displayEndDate = new Carbon($end_date);

        $title = 'Total Sales For Period: ' . $displayStartDate->toFormattedDateString() . ' To: ' . $displayEndDate->toFormattedDateString() . ' For ' . $shop_name;


        $chart_data = $query->get();

        /*get sum amount*/
        $sum = $sum_query->select(DB::raw('SUM(payment.amount) as amount'))
            ->first();
        if (count($sum)) {
            $sum = $sum->amount;
        } else {
            $sum = 0;
        }
        $total_sales = number_format($sum, 2);

        /*shops*/
        $shops = [];
        return $dataTable->render('SuperAdmin.reports.sales.sales_summary', compact('total_sales', 'chart_data', 'start_date', 'end_date', 'shops', 'title'));
    }

    public function salesByItem(SalesByItemReportDataTable $dataTable, Request $request)
    {
        $query = $dataTable->query();
        $start_date = Carbon::create()->subMonth();
        $end_date = Carbon::create();

        $top5 = $dataTable->getTop5()->get();

        $shop_name = 'All Shops';
        if ($request->has('shop') && $request->get('shop')) {
            $shop_id = $request->get('shop');
            $shop = shop::find($shop_id);
            $hq_id = $shop->HQID;
            $local_id = $shop->local_id;
            $shop_name = $shop->shop_name;
        }
        if ($request->has('start_date') && $request->get('start_date') && $request->has('end_date') && $request->get('end_date')) {
            $start_date = new Carbon($request->get('start_date'));
            $end_date = new Carbon($request->get('end_date'));
        }
        $displayStartDate = new Carbon($start_date);
        $displayEndDate = new Carbon($end_date);

        $title = 'Sales By Item For Period: ' . $displayStartDate->toFormattedDateString() . ' To: ' . $displayEndDate->toFormattedDateString() . ' For ' . $shop_name;


        $chart_data = $query->orderBy('total_amount', 'desc')
            ->limit(5)->get();

        /*shops*/
        $shops = [];
        return $dataTable->render('SuperAdmin.reports.sales.salesByItem', compact('chart_data', 'start_date', 'end_date', 'shops', 'title', 'top5'));
    }
}
