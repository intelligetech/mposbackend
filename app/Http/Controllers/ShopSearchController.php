<?php

namespace App\Http\Controllers;

use App\shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class ShopSearchController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /* Super Admin client select search*/
    public function shopSearch()
    {

        $page = Input::get('page');
        $resultCount = 500;
        $search = Input::get('search');


        $offset = ($page - 1) * $resultCount;

        $all_shops = shop::where('shop_name', 'LIKE', '%' . $search . '%')
            ->where('shop_name', '!=', "")
            ->orderBy('shop_name')
            ->count('id');
        $count = $all_shops;
        $shops = shop::where('shop_name', 'LIKE', '%' . $search . '%')
            ->where('shop_name', '!=', "")
            ->orderBy('shop_name')
            ->skip($offset)
            ->take($resultCount)
            ->select('id', 'shop_name as text')
            ->get();

        $endCount = $offset + $resultCount;
        $morePages = $count > $endCount;
        $data = array(
            "results" => $shops,
            "pagination" => array(
                "more" => $morePages
            )
        );
        return response()->json($data);;
    }

    /*HQ Admin client select search*/
    public function adminShopSearch()
    {

        $hq_id  = Auth::user()->HQID;

        $page = Input::get('page');
        $resultCount = 500;
        $search = Input::get('search');


        $offset = ($page - 1) * $resultCount;

        $all_shops = shop::where('shop_name', 'LIKE', '%' . $search . '%')
            ->where('shop_name', '!=', "")
            ->where('HQID',$hq_id)
            ->orderBy('shop_name')
            ->count('id');
        $count = $all_shops;
        $shops = shop::where('shop_name', 'LIKE', '%' . $search . '%')
            ->where('shop_name', '!=', "")
            ->where('HQID',$hq_id)
            ->orderBy('shop_name')
            ->skip($offset)
            ->take($resultCount)
            ->select('id', 'shop_name as text')
            ->get();

        $endCount = $offset + $resultCount;
        $morePages = $count > $endCount;
        $data = array(
            "results" => $shops,
            "pagination" => array(
                "more" => $morePages
            )
        );
        return response()->json($data);;
    }
}
