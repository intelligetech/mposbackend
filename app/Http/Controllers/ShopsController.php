<?php

namespace App\Http\Controllers;

use App\DataTables\ShopsDataTable;
use Illuminate\Http\Request;

class ShopsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(ShopsDataTable $dataTable)
    {
        return $dataTable->render('SuperAdmin.shops.index');

    }
}
