<?php
/**
 * Created by PhpStorm.
 * User: Githire
 * Date: 3/1/2018
 * Time: 9:55 AM
 */

namespace App\Http\Controllers\UtilityFunctionsController;


class UtilityFunctionsController
{
    public static function checkPhoneNumber($phonenum)
    {
        $phonenum = str_replace(' ', '', $phonenum);
        if (strlen($phonenum) == 12 && substr($phonenum, 0, 4) == "2547") {
            return $phonenum;
        } else if (strlen($phonenum) == 10 && substr($phonenum, 0, 2) == "07") {
            return "254" . substr($phonenum, 1);
        } else if (strlen($phonenum) == 9 && substr($phonenum, 0, 1) == "7") {
            return "254" . $phonenum;
        } else {
            return false;
        }
    }
}