<?php

namespace App\Http\Controllers;

use App\bill;
use App\categories;
use App\departments;
use App\order_supply;
use App\products;
use App\roles;
use App\shop;
use App\supplier;
use App\sync;
use App\tax;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Mockery\Exception;

class WebController extends Controller
{

    public function __construct()
    {

    }

    public function users(Request $request)
    {
        $value = $request->session()->get('HQID');

        if ($value == '' || $value == null) {
            return redirect('/');
        } else {
            $users = User::leftjoin('shop', 'shop.local_id', '=', 'users.shop_id')
                ->leftjoin('roles', 'roles.id', '=', 'users.role_id')
                ->select('users.*', 'roles.role_name', 'shop.shop_name')
                ->where(['users.HQID' => $value, 'shop.HQID' => $value])
                ->paginate(10);
            return view('web.users', compact('users'));
        }

    }

    public function suppliers(Request $request)
    {
        $value = $request->session()->get('HQID');

        if ($value == '' || $value == null) {
            return redirect('/');
        } else {
            $suppliers = supplier::leftjoin('shop', 'shop.local_id', '=', 'supplier.shop_id')
                ->select('supplier.*', 'shop.shop_name')
                ->where(['supplier.HQID' => $value, 'shop.HQID' => $value])
                ->paginate(10);
            return view('web.suppliers', compact('suppliers'));
        }
    }

    public function shops(Request $request)
    {
        $value = $request->session()->get('HQID');

        if ($value == '' || $value == null) {
            return redirect('/');
        } else {
            $shops = shop::where('HQID', $value)->paginate(10);
            return view('web.shops', compact('shops'));
        }
    }

    public function departments(Request $request)
    {
        $value = $request->session()->get('HQID');

        if ($value == '' || $value == null) {
            return redirect('/');
        } else {
            $departments = departments::leftjoin('shop', 'shop.local_id', '=', 'departments.shop_id')
                ->select('departments.*', 'shop.shop_name')
                ->where(['departments.HQID' => $value, 'shop.HQID' => $value])
                ->paginate(10);
            return view('web.departments', compact('departments'));
        }
    }

    public function categories(Request $request)
    {
        $value = $request->session()->get('HQID');

        if ($value == '' || $value == null) {
            return redirect('/');
        } else {
            $categories = categories::leftjoin('departments', 'departments.local_id', '=', 'categories.department_id')
                ->leftjoin('shop', 'shop.local_id', '=', 'categories.shopId')
                ->select('categories.*', 'departments.department_name', 'shop.shop_name')
                ->where(['categories.HQID' => $value, 'shop.HQID' => $value, 'departments.HQID' => $value])
                ->paginate(10);
            return view('web.categoriess', compact('categories'));
        }
    }

    public function products(Request $request)
    {
        $value = $request->session()->get('HQID');

        if ($value == '' || $value == null) {
            return redirect('/');
        } else {
            $products = products::leftjoin('users', 'users.local_id', '=', 'products.user_id')
                ->leftjoin('categories', 'categories.local_id', '=', 'products.category_id')
                ->leftjoin('shop', 'shop.local_id', '=', 'products.shopID')
                ->leftjoin('tax', 'tax.local_id', '=', 'products.tax_id')
                ->select('products.*', 'categories.category_name', 'tax.tax_name', 'shop.shop_name')
                ->where(['products.HQID' => $value, 'categories.HQID' => $value, 'shop.HQID' => $value, 'tax.HQID' => $value, 'users.HQID' => $value])
                ->paginate(10);
            return view('web.products', compact('products'));
        }
    }

    public function roles(Request $request)
    {
        $value = $request->session()->get('HQID');

        if ($value == '' || $value == null) {
            return redirect('/');
        } else {
            $roles = roles::paginate(10);
            return view('web.roles', compact('roles'));
        }
    }

    public function tax(Request $request)
    {
        $value = $request->session()->get('HQID');

        if ($value == '' || $value == null) {
            return redirect('/');
        } else {
            $taxes = tax::leftjoin('shop', 'shop.local_id', '=', 'tax.shop_id')
                ->select('tax.*', 'shop.shop_name')
                ->where(['tax.HQID' => $value, 'shop.HQID' => $value])
                ->paginate(10);
            return view('web.tax', compact('taxes'));
        }
    }

    public function orders(Request $request)
    {
        $value = $request->session()->get('HQID');

        if ($value == '' || $value == null) {
            return redirect('/');
        } else {
            $orders = order_supply::leftjoin('shop', 'shop.local_id', '=', 'order_supply.shop_id')
                ->leftjoin('supplier', 'supplier.local_id', '=', 'order_supply.supplier_id')
                ->select('order_supply.*', 'shop.shop_name', 'supplier.supplier_name')
                ->where(['order_supply.HQID' => $value, 'supplier.HQID' => $value, 'shop.HQID' => $value])
                ->paginate(10);
            return view('web.orders', compact('orders'));
        }
    }

    public function logout(Request $request)
    {
        $request->session()->forget('HQID');
        $request->session()->forget('user_id');
        $request->session()->forget('local_user_id');
        Auth::logout();
        return redirect('/');

    }

    public function salesReport(Request $request)
    {
        $value = $request->session()->get('HQID');

        if ($value == '' || $value == null) {
            return redirect('/');
        } else {
            $shops = shop::where('HQID', $value)->pluck('shop_name', 'local_id');
            return view('web.sales', compact('shops'));
        }
    }

    public function generateSale(Request $request)
    {
        $value = $request->session()->get('HQID');
        $shops = shop::where('HQID', $value)->pluck('shop_name', 'local_id');

        $start = $request['start-date'];
        $end = $request['end-date'];
        $rules = array(
            'start-date' => 'required',
            'end-date' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('list/sales/report')->withErrors($validator);
        }
        $h = $request->input('shop_id');
        $transactions = bill::leftjoin('users', 'users.local_id', '=', 'bill.user_id')
            ->leftjoin('shop', 'shop.local_id', '=', 'bill.shopID')
            ->leftjoin('bill_products', 'bill_products.bill_id', '=', 'bill.local_id')
            ->leftjoin('payment', 'payment.bill_id', '=', 'bill.local_id')
            ->leftjoin('products', 'products.local_id', '=', 'bill_products.product_id')
            ->where(DB::raw('date(payment.created_at)'), '>=', $start)
            ->where(DB::raw('date(payment.created_at)'), '<=', $end)
            ->where(['bill.shopID' => $h, 'bill.HQID' => $value, 'shop.HQID' => $value, 'users.HQID' => $value])
            ->select('users.username', 'shop.shop_name', 'products.product_name', 'bill.type', 'bill_products.qty', 'bill_products.price', 'payment.time')
            ->get();
        return view('web.sales', compact('shops', 'start', 'end', 'transactions'));
    }

    public function uploadDept(Request $request)
    {
        $value = $request->session()->get('HQID');
        $shops = shop::where('HQID', $value)->pluck('shop_name', 'local_id as id');
        return view('web.uploadDept', compact('shops'));
    }

    public function saveDept(Request $request)
    {
        if ((Input::file('file') == null)) {
            Session::flash('error', 'Please select a file to upload');
            return redirect()->back()->withInput();
        }
        $value = $request->session()->get('HQID');
        $max_local_id = departments::where('HQID', '=', $value)
            ->where('shop_id', '=', $request->input('shop_id'))
            ->max('local_id');
        if (count($max_local_id)) {
            $local_id = $max_local_id + 1;
        } else {
            $local_id = 1;
        }
        Excel::load(Input::file('file'), function ($reader) use ($request, $value, $local_id) {
            DB::beginTransaction();
            try {
                $errors = [];
                $user_id = $request->session()->get('local_user_id');
                $sync_id = sync::create()->id;
                foreach ($reader->get() as $r) {
                    // dd($reader->toArray());
                    unset($r['']);
                    //  dd($r->toArray());
                    $validator = Validator::make($r->toArray(), [
                        'department_name' => 'required',
                        'code' => 'required',
                    ]);

                    //dd($r);
                    if ($validator->fails()) {
                        $message = 'validator fails';
                        array_push($errors, $message);
                        DB::rollback();
                        redirect('upload/departments')
                            ->withErrors($validator)
                            // ->withErrors($validator1)
                            ->withInput();
                    } else {
                        $existing_rec = departments::where('HQID', '=', $value)
                            ->where('shop_id', '=', $request->input('shop_id'))
                            ->where('department_name', '=', $r->department_name)
                            ->get();
                        if (count($existing_rec)) {
                            $message = 'existing records';
                            array_push($errors, $message);
                            DB::rollback();
                            Session::flash('error', 'Upload failed. Record with name ' . $r->department_name . ' already exists ');
                            return redirect()->back();
                        } else {
                            departments::create([
                                'local_id' => $local_id,
                                'department_name' => $r->department_name,
                                'code' => $r->code,
                                'description' => $r->description,
                                'user_id' => $user_id,
                                'HQID' => $value,
                                'sync' => 0,
                                'sync_id' => $sync_id,
                                'shop_id' => $request->input('shop_id'),
                                'shopID' => $request->input('shop_id')
                            ]);
                        }
                    }
                    $local_id++;
                }
            } catch (\Exception $e) {
                $message = 'db errors';
                array_push($errors, $message);
                DB::rollback();
            }
            if (!count($errors)) {
                DB::commit();
                Session::flash('success_message', 'You have uploaded the data successfully.');
            }
        });
        return redirect()->back();
    }

    public function uploadCategories(Request $request)
    {
        $value = $request->session()->get('HQID');
        $shops = shop::where('HQID', $value)->pluck('shop_name', 'local_id as id');
        $all_shops = shop::where('HQID', $value)->get();

        foreach ($all_shops as $shop) {
            $depts[$shop->local_id] = departments::leftjoin('shop', 'shop.local_id', '=', 'departments.shopID')
                ->where('departments.HQID', $value)
                ->where('departments.shop_id', '=', $shop->local_id)
                ->orderBy('department_name', 'asc')
                ->pluck('department_name', 'departments.local_id as id');
        }
        $none = [];
        //return json_encode($depts);
        if (!count($shops)) {
            Session::flash('error', 'No shops found');
            $depts = [];
            return view('web.uploadCategories', compact('shops', 'depts', 'none'));

        }
        return view('web.uploadCategories', compact('shops', 'depts', 'none'));
    }

    public function saveCategories(Request $request)
    {

        if ((Input::file('file') == null)) {
            Session::flash('error', 'Please select a file to upload');
            return redirect()->back()->withInput();
        }

        if (($request['shop_id'] == null) || ($request['department_id'] == null)) {
            Session::flash('error', 'Please select required fields');
            return redirect()->back()->withInput();
        }

        $HQID = $request->session()->get('HQID');

        /*get last record from the shop to determine next local_id*/
        $max_local_id = categories::where('HQID', '=', $HQID)
            ->where('shopID', '=', $request->input('shop_id'))
            ->max('local_id');
        if (count($max_local_id)) {
            $local_id = $max_local_id + 1;
        } else {
            $local_id = 1;
        }
        Excel::load(Input::file('file'), function ($reader) use ($request, $HQID, $local_id) {
            $department_id = $request['department_id'];
            $shop_id = $request['shop_id'];
            DB::beginTransaction();
            try {
                $errors = [];
                $user_id = $request->session()->get('local_user_id');
                $sync_id = sync::create()->id;
                foreach ($reader->get() as $r) {
                    // dd($reader->toArray());
                    unset($r['']);
                    //  dd($r->toArray());
                    $validator = Validator::make($r->toArray(), [
                        'category_name' => 'required'
                    ]);

                    //dd($r);
                    if ($validator->fails()) {
                        DB::rollback();
                        $message = 'validator errors';
                        array_push($errors, $message);
                        redirect('upload/categories')
                            ->withErrors($validator)
                            // ->withErrors($validator1)
                            ->withInput();
                    } else {
                        $existing_rec = categories::where('HQID', '=', $HQID)
                            ->where('shopID', '=', $request->input('shop_id'))
                            ->where('category_name', '=', $r->category_name)
                            ->get();
                        if (count($existing_rec)) {
                            DB::rollback();
                            $message = 'duplicate record';
                            array_push($errors, $message);
                            Session::flash('error', 'Upload failed. Record with name ' . $r->category_name . ' already exists ');
                            return redirect()->back();
                        } else {
                            categories::create([
                                'local_id' => $local_id,
                                'category_name' => $r->category_name,
                                'description' => $r->description,
                                'department_id' => $department_id,
                                'user_id' => $user_id,
                                'HQID' => $HQID,
                                'sync' => 0,
                                'sync_id' => $sync_id,
                                'shopID' => $shop_id
                            ]);
                        }
                    }
                    $local_id++;
                }
            } catch (\Exception $e) {
                DB::rollback();
                $message = 'db errors';
                array_push($errors, $message);
                Session::flash('error', 'Upload failed.');
                return redirect()->back();
            }

            if (!count($errors)) {
                DB::commit();
                Session::flash('success_message', 'You have uploaded the data successfully.');
            }
        });
        return redirect()->back();
    }

    /*Upload products*/
    public function uploadProducts(Request $request)
    {
        $value = $request->session()->get('HQID');
        $shops = shop::where('HQID', $value)->pluck('shop_name', 'local_id as id');
        $all_shops = shop::where('HQID', $value)->get();

        foreach ($all_shops as $shop) {
            $depts[$shop->local_id] = departments::leftjoin('shop', 'shop.local_id', '=', 'departments.shopID')
                ->where('departments.HQID', $value)
                ->where('departments.shop_id', '=', $shop->local_id)
                ->orderBy('department_name', 'asc')
                ->pluck('department_name', 'departments.local_id as id');
            $categories[$shop->local_id] = categories::leftjoin('shop', 'shop.local_id', '=', 'categories.shopID')
                ->where('categories.HQID', $value)
                ->where('categories.shopID', '=', $shop->local_id)
                ->orderBy('category_name', 'asc')
                ->pluck('category_name', 'categories.local_id as id');
        }
        $none = [];
        return view('web.uploadProducts', compact('shops', 'categories', 'depts', 'none'));
    }

    /*save uploaded products*/
    public function saveProducts(Request $request)
    {
        if ((Input::file('file') == null)) {
            Session::flash('error', 'Please select a file to upload');
            return redirect()->back()->withInput();
        }

        if (($request['shop_id'] == null) || ($request['category_id'] == null)) {
            Session::flash('error', 'Please select required fields');
            return redirect()->back()->withInput();
        }

        $HQID = $request->session()->get('HQID');

        /*get last record from the shop to determine next local_id*/
        $max_local_id = products::where('HQID', '=', $HQID)
            ->where('shopID', '=', $request->input('shop_id'))
            ->max('local_id');
        if (count($max_local_id)) {
            $local_id = $max_local_id + 1;
        } else {
            $local_id = 1;
        }
        Excel::load(Input::file('file'), function ($reader) use ($request, $HQID, $local_id) {
            $category_id = $request['category_id'];
            $shop_id = $request['shop_id'];
            DB::beginTransaction();
            try {
                $errors = [];
                $user_id = $request->session()->get('local_user_id');
                $sync_id = sync::create()->id;
                foreach ($reader->get() as $r) {
                    // dd($reader->toArray());
                    unset($r['']);
                    //  dd($r->toArray());
                    $validator = Validator::make($r->toArray(), [
                        'product_name' => 'required',
                        'price' => 'required|numeric',
                        'initial_stock' => 'required|numeric',
                        'unit_of_measure' => 'required',
                        'tax_type' => 'required',
                        'buying_price' => 'required'
                    ]);

                    //dd($r);
                    if ($validator->fails()) {
                        DB::rollback();
                        $message = 'validator errors';
                        array_push($errors, $message);
                        redirect('upload/products')
                            ->withErrors($validator)
                            // ->withErrors($validator1)
                            ->withInput();
                    } else {
                        $tax = tax::where('tax_name', '=', $r->tax_type)
                            ->where('HQID', '=', $HQID)
                            ->where('shopID', '=', $request->input('shop_id'))
                            ->first();
                        if (!count($tax)) {
                            DB::rollback();
                            $message = 'no tax settings for tax name: ' . $r->tax_type;
                            array_push($errors, $message);
                            Session::flash('error', 'Upload failed. no tax settings for tax name: ' . $r->tax_type);
                            return redirect()->back();
                        }
                        $tax_id = $tax->local_id;
                        $existing_rec = products::where('HQID', '=', $HQID)
                            ->where('shopID', '=', $request->input('shop_id'))
                            ->where('product_name', '=', $r->product_name)
                            ->get();
                        if (count($existing_rec)) {
                            DB::rollback();
                            $message = 'duplicate record';
                            array_push($errors, $message);
                            Session::flash('error', 'Upload failed. Record with name ' . $r->product_name . ' already exists ');
                            return redirect()->back();
                        } else {
                            products::create([
                                'local_id' => $local_id,
                                'product_name' => $r->product_name,
                                'description' => $r->description,
                                'price' => $r->price,
                                'initial_stock' => $r->initial_stock,
                                'unit_of_measure' => $r->unit_of_measure,
                                'tax_id' => $tax_id,
                                'category_id' => $category_id,
                                'user_id' => $user_id,
                                'HQID' => $HQID,
                                'sync' => 0,
                                'sync_id' => $sync_id,
                                'shopID' => $shop_id,
                                'buying_price' => $r->buying_price
                            ]);
                        }
                    }
                    $local_id++;
                }
            } catch (\Exception $e) {
                DB::rollback();
                $message = 'db errors';
                array_push($errors, $message);
                Session::flash('error', 'Upload failed.');
                return redirect()->back();
            }

            if (!count($errors)) {
                DB::commit();
                Session::flash('success_message', 'You have uploaded the data successfully.');
            }
        });
        return redirect()->back();
    }
}
