<?php

namespace App\Http\Controllers;

//use App\people;
use App\batch;
use App\bill;
use App\bill_products;
use App\BillingCycle;
use App\categories;
use App\customers;
use App\departments;
use App\discounts;
use App\headquarter;
use App\Http\Controllers\UtilityFunctionsController\UtilityFunctionsController;
use App\order_supply;
use App\orders;
use App\payment;
use App\people;
use App\price_history;
use App\ProductDiscount;
use App\products;
use App\received_supply;
use App\roles;
use App\shop;
use App\Status;
use App\stock_tracker;
use App\Subscription;
use App\SubscriptionPayments;
use App\supplier;
use App\sync;
use App\tax;
use App\test2;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\MpesaControllers\MpesaCustom;
use App\MpesaPayment;
use App\StkPush;
use Log;

class MobileAppController extends Controller
{
    protected $mpesaStkPushCallBackURL;
    protected $mpesaBusinessCode;
    protected $mpesaOnlinePassword;
    protected $mpesaTestPhoneNumber;
    public function __construct()
    {
        $this->mpesaStkPushCallBackURL = config('app.mpesaStkPushCallBackURL');
        $this->mpesaBusinessCode = config('app.mpesaBusinessCode');
        $this->mpesaOnlinePassword = config('app.mpesaOnlinePassword');
        $this->mpesaTestPhoneNumber = config('app.mpesaTestPhoneNumber');
        //$this->middleware('jwt.auth', ['except' => ['authenticate', 'signUp', 'registerHq', 'insert_data', 'getsyncID',
        // 'adminLogin']]);

    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            // verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // if no errors are encountered we can return a JWT
        return response()->json(compact('token'));
    }

    public function getAuthenticatedUser()
    {
        try {

            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        // the token is valid and we have found the user via the sub claim
        return response()->json(compact('user'));
    }

    public function registerHq(Request $request)
    {
        $name = $request['name'];
        $location = $request['location'];
        $description = $request['description'];

        $headquarter = headquarter::create([
            'name' => $name,
            'location' => $location,
            'description' => $description
        ]);
        if (!empty($headquarter)) {
            $HQID = $headquarter->id;
            $hq_name = $headquarter->name;
            return array('message' => 'success', 'HQID' => $HQID, 'hq_name' => $hq_name);
        } else {
            return array('message' => 'error');
        }

    }

    public function getsyncID()
    {
        $sync = sync::create();
        $sync_id = $sync->id;
        if ($sync_id != null) {
            return array('message' => 'success', 'sync_id' => $sync_id);
        } else {
            return array('message' => 'error');
        }

    }

    public function insert_data(Request $request)
    {
        $data = $request['data'];
        $table_name = $request['table_name'];
        $columns = $request['columns'];
        $user_id = $request['user_id'];
        $HQID = $request['HQID'];
        $action = $request['action'];
        $sync_status = $request['sync_status'];
        $shopID = $request['shopID'];


        $models_array = [
            'people' => new people(),
            'test2' => new test2(),
            'batch' => new batch(),
            'bill' => new bill(),
            'bill_products' => new bill_products(),
            'categories' => new categories(),
            'customers' => new customers(),
            'departments' => new departments(),
            'discounts' => new discounts(),
            //'headquarter' => new headquarter(),
            'order_supply' => new order_supply(),
            'orders' => new orders(),
            'payment' => new payment(),
            'price_history' => new price_history(),
            'products' => new products(),
            'received_supply' => new received_supply(),
            'roles' => new roles(),
            'shop' => new shop(),
            'stock_tracker' => new stock_tracker(),
            'supplier' => new supplier(),
            'tax' => new tax(),
            'users' => new User(),
            'product_discounts' => new ProductDiscount(),
            'subscriptionPayments' => new SubscriptionPayments(),
        ];
        /*if data is not empty*/
        if (!empty($data)) {
            /*check action ie new record-0 or update-2*/
            if ($sync_status == "start") {
                $sync = sync::create();
                $sync_id = $sync->id;
                $sync_time = $sync->created_at;
            } elseif ($sync_status == "ongoing") {
                $sync_id = $request['sync_id'];
                if ($sync_id != null) {
                    $sync_time = sync::find($sync_id)->first()->created_at;
                } else {
                    $sync = sync::create();
                    $sync_id = $sync->id;
                    $sync_time = $sync->created_at;
                }
            }

            if ($action == 0) {
                //new record
                /*loop through the records to be inserted*/
                DB::beginTransaction();
                foreach ($data as $record) {
                    //use try catch block to catch sql exception
                    //check if table is users or shops to not set the shopID variable
                    if (($table_name != 'users') && ($table_name != 'shop') &&
                        ($table_name != 'subscriptionPayments')) {
                        $record['shopID'] = $shopID;
                    }
                    if ($table_name == 'payment') {
                        $record['time'] = new Carbon($record['time']);
                    }
                    $record['sync_id'] = $sync_id;
                    $record['user_id'] = $user_id;
                    $record['HQID'] = $HQID;
                    try {
                        $insert_record = $models_array[$table_name]::create($record);
                    } catch (\Exception $e) {
                        DB::rollBack();
                        return array('message' => 'insert_error', 'error' => $e->getMessage(), 'table' => $table_name, 'insert' => $models_array);
                        //return($e);
                    }

                }
                DB::commit();
                return array('result' => $data, 'message' => 'success', 'sync_time' => $sync_time, 'action' => $action, 'count' => count($data), 'sync_id' => $sync_id);
            } else if ($action == 2) {
                //update record
                DB::beginTransaction();
                foreach ($data as $record) {
                    //use try catch block to catch sql exception
                    try {
                        /*update record*/
                        /*check if table is users or shops*/
                        if (($table_name != 'users') && ($table_name != 'shop') &&
                            ($table_name != 'subscriptionPayments')) {
                            $update_record = $models_array[$table_name]::where(['local_id' => $record['local_id'], 'shopID' => $shopID, 'HQID' => $HQID]);
                        } else {
                            $update_record = $models_array[$table_name]::where(['local_id' => $record['local_id'], 'HQID' => $HQID]);
                        }
                        $rec_count = $update_record->get();
                        if (count($rec_count)) {
                            $update_record->update($record);
                        } else {
                            if (($table_name != 'users') && ($table_name != 'shop')&&
                                ($table_name != 'subscriptionPayments')) {
                                $record['shopID'] = $shopID;
                            }
                            if ($table_name == 'bill') {
                                if ($record['total_amount'] == null) {
                                    $record['total_amount'] = 0;
                                }
                            }

                            $record['sync_id'] = $sync_id;
                            $record['user_id'] = $user_id;
                            $record['HQID'] = $HQID;
                            $insert_record = $models_array[$table_name]::create($record);
                        }
                    } catch (\Exception $e) {
                        DB::rollBack();
                        return array('message' => 'update_error', 'error' => $e->getMessage());
                        //return ($e);
                    }

                }
                DB::commit();
                return array('result' => $data, 'message' => 'success', 'sync_time' => $sync_time, 'table_name' => $table_name, 'action' => $action, 'count' => count($data));
                //return array('message' => 'updated', 'result' => $data);

            }


            //return $data;
        }
        return array('result' => $data, 'table_name' => $columns, 'action' => $action, 'count' => count($data));

    }

    public function adminLogin(Request $request)
    {
        $username = $request['username'];
        $password = $request['password'];
        $user = User::where(['username' => $username, 'password' => $password])->first();
        if (!empty($user)) {
            //dd( $user->password );
            $shops = shop::where(['HQID' => $user->HQID])->get();
            // $shops = DB::table('shop')->where('HQID',$user->HQID)->get();
            //$shops = $shops;
            //return $shops;
            if (!empty($shops)) {
                return response()->json(['message' => 'success', 'user' => $user, 'shops' => $shops]);
            } else {
                return array('message' => 'error');
            }
        } else {
            return array('message' => 'error');
        }
    }

    /*get sync data for a given shop*/
    public function downloadSync(Request $request)
    {
        $admin = $request['admin'];
        if (count($admin)) {
            $HQID = $admin['HQID'];
        } else {
            $HQID = $request['HQID'];
        }
        $shop_id = $request['shop_id'];
        if (count($shop_id)) {
            $thisshop = DB::table('shop')->where(['id' => $shop_id])->first();
        } else {
            $thisshop = DB::table('shop')->where(['HQID' => $HQID])->first();
        }
        $shopID = $thisshop->local_id;

        //return array('shop_id' => $HQID);

        $tables = DB::select('SHOW TABLES');
        $all_tables = [];
        $all_data = [];
        $new_tables = [];
        foreach ($tables as $table) {
            array_push($all_tables, $table->Tables_in_mpos);
        }

        foreach ($all_tables as $table) {
            /*skip some tables that'll remain on the server*/
            if ($table == 'headquarter'
                || $table == 'migrations'
                || $table == 'sync'
                || $table == 'password_resets'
                || $table == 'roles'
                || $table == 'activity_log'
                || $table == 'billing_cycle'
                || $table == 'mpesa_payments'
                || $table == 'status'
                || $table == 'stkpush'
                || $table == 'subscriptions'
            ) {
                continue;
            }
            $col_names = Schema::getColumnListing($table);
            $new_cols = [];
            /*skip some columns not required at the phone app*/
            foreach ($col_names as $col) {
                if ($col == 'id'
                    || $col == 'created_at'
                    || $col == 'updated_at'
                    || $col == 'sync_id'
                    || $col == 'sync'
                    ||$col == 'remember_token'
                    || $col == 'shopID'
                ) {
                    continue;
                }
                if ($col == 'local_id') {
                    $col = 'local_id as id';
                }
                if (($col == 'HQID') && ($table != 'shop') && ($table != 'users')) {
                    continue;
                }
                if (($col == 'user_id') && ($table != 'batch') && ($table != 'stock_tracker')) {
                    continue;
                }
                array_push($new_cols, $col);
            }
            /*check if table is users or shops and use a different query for the rest of the tables*/
            if (($table == 'users')) {
                $table_data = DB::table($table)->select($new_cols)->where(['HQID' => $HQID])->get();
            } elseif (($table == 'shop')) {
                $table_data = DB::table($table)->select($new_cols)->where(['HQID' => $HQID])->get();
            } elseif (($table == 'subscriptionPayments')) {
                $table_data = DB::table($table)->select($new_cols)->where(['HQID' => $HQID])->get();
            } else {
                $table_data = DB::table($table)
                    ->select($new_cols)
                    ->where(['HQID' => $HQID, 'shopID' => $shopID])
                    ->get();
            }
            if ($table_data->count()) {
                //return $table_data;
                $all_data[$table]['columns'] = $new_cols;
                $all_data[$table]['data'] = $table_data;
                array_push($new_tables, $table);
            }
        }
        if (!empty($all_data)) {
            return array('message' => 'success', 'download_data' => $all_data,
                'tables' => $new_tables, 'HQID' => $HQID);
        } else {
            return array('message' => 'error');
        }

    }

    public function checkUsername(Request $request)
    {
        $name = $request['name'];

        $user = User::where('username', $name)->first();
        if (count($user) == 0) {
            return array('message' => 'success', 'dat' => count($user), 'f' => $user);
        } else {
            return array('message' => 'error', 'dat' => count($user), 'f' => $user);
        }
    }

    public function checkActivation(Request $request)
    {
        $user = User::where('username', $request['username'])->first();
        $subscription = Subscription::join('mpesa_payments','mpesa_payments.subscription_id','=','subscriptions.id')
            ->join('status', 'status.id', '=', 'subscriptions.status_id')
            ->where('subscriptions.hq_id',$user->HQID)
            ->where('status_id', '!=', Status::where('code', 'SUBCANCELLED')->first()->id)
            ->select('mpesa_payments.expiry_date')
            ->first();

        if (count($subscription) != 0) {
            return array('message' => 'success', 'dat' => $subscription);
        } else {
            return array('message' => 'error', 'dat' => $subscription);
        }

    }

    public function save(Request $request)
    {
        try {
            DB::beginTransaction();
            //  $user = Auth::user();
            $user = User::where('username', $request['username'])
                ->first();
            $user_id = $user->id;
            $hq_id = $user->HQID;

            //  $inputPhone = $request->input('phone_number');
            $inputPhone = $user->phone_number;
            if (!count($inputPhone)) {
                $inputPhone = $user->phone_number;
            }

            $inputPhone = UtilityFunctionsController::checkPhoneNumber($inputPhone);

            if (!$inputPhone) {
                $custom = 'Incorrect Phone Number Format. Use 2547********';
                return response()->json(compact('custom'), 400);
            }


            $existingSubscription = Subscription::where('hq_id', $hq_id)
                ->where('status_id', '!=', Status::where('code', 'SUBCANCELLED')->first()->id)
                ->first();

            if (count($existingSubscription)) {
                $existingSubscription->update(['status_id'=>Status::where('code', 'SUBCANCELLED')->first()->id]);
            }

            $time = Carbon::now();
            $time = $time->format('Ymdhis');
            $billing_cycle = BillingCycle::where('name', 'Yearly')->first();
            $billingAmount = number_format($billing_cycle->amount);
            $businessShortCode = $this->mpesaBusinessCode;
            $password = $this->mpesaOnlinePassword;
            $Timestamp = $time;
            $transactionType = 'CustomerPayBillOnline';
            $amount = $billingAmount;
            $partyA = $inputPhone;
            $partyB = $this->mpesaBusinessCode;
            $phoneNumber = $inputPhone;
            $callBackURL = $this->mpesaStkPushCallBackURL;
            $accountReference = $hq_id;
            $transactionDesc = 'Lyte Shop' . $billing_cycle->name . ' Subscription';
            $remark = 'MYohMY';
            $CheckoutRequestID = 'ws_CO_28022018102815517';


            //$stkQuery = $mpesaCustom->STKPushQuery('', $CheckoutRequestID, $BusinessShortCode, $Password, $Timestamp);
            //dd($stkPush);

            if (count($billing_cycle)) {
                $billing_cycle_id = $billing_cycle->id;
            } else {
                $billing_cycle_id = 1;
            }
            $status = Status::where('code', 'SUBPENDING')->first();
            $status_id = $status->id;
            $subscription = Subscription::create([
                'hq_id' => $hq_id,
                'billing_cycle_id' => $billing_cycle_id,
                'status_id' => $status_id,
                'created_by' => $user_id
            ]);
            $subscription_id = $subscription->id;
            /*stkpush*/
            try {
                $mpesaCustom = new MpesaCustom();
                $stkPush = $mpesaCustom->STKPushSimulation(
                    $businessShortCode,
                    $password,
                    $transactionType,
                    $amount,
                    $partyA,
                    $partyB,
                    $phoneNumber,
                    $callBackURL,
                    $accountReference,
                    $transactionDesc,
                    $remark
                );

                Log::info(json_encode($stkPush));

                /*store stkpush for verification in callback*/
                $stkPushTable = StkPush::create([
                    'hq_id' => $hq_id,
                    'subscription_id' => $subscription_id,
                    "merchantRequestID" => $stkPush->MerchantRequestID,
                    "checkoutRequestID" => $stkPush->CheckoutRequestID,
                    "responseCode" => $stkPush->ResponseCode,
                    "responseDescription" => $stkPush->ResponseDescription,
                    "customerMessage" => $stkPush->CustomerMessage
                ]);

            } catch (\Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                $file = $e->getFile();
                $code = $e->getCode();
                $line = $e->getLine();
                $trace = $e->getTraceAsString();
                $custom = 'Error STK';
                return response()->json(compact('custom', 'message', 'code', 'file', 'line', 'e', 'trace'), 400);
            }
            DB::commit();
            $custom = 'Success';
            return array('custom' => 'Success');
        } catch (\Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            $file = $e->getFile();
            $code = $e->getCode();
            $line = $e->getLine();
            $trace = $e->getTraceAsString();
            $custom = 'Error';
            return response()->json(compact('custom', 'message', 'code', 'file', 'line', 'e', 'trace'), 400);
        }
    }

}
