<?php
/**
 * Created by PhpStorm.
 * User: Githire
 * Date: 2/28/2018
 * Time: 8:30 AM
 */

namespace App\Http\Controllers\MpesaControllers;


use GuzzleHttp\Client;
use Safaricom\Mpesa\Mpesa;

class MpesaCustom extends Mpesa
{
    protected static $mpesaConsumerSecret, $mpesaEnv;
    protected static $mpesaConsumerKey = 'me';

    public function __construct()
    {
        self::$mpesaConsumerKey = config('app.mpesaConsumerKey');
        self::$mpesaConsumerSecret = config('app.mpesaConsumerSecret');
        self::$mpesaEnv = config('app.mpesaEnv');
    }

    public static function setVariables()
    {
        self::$mpesaConsumerKey = config('app.mpesaConsumerKey');
        self::$mpesaConsumerSecret = config('app.mpesaConsumerSecret');
        self::$mpesaEnv = config('app.mpesaEnv');
    }

    public static function generateLiveToken()
    {
        $consumer_key = self::$mpesaConsumerKey;
        $consumer_secret = self::$mpesaConsumerSecret;

        if (!isset($consumer_key) || !isset($consumer_secret)) {
            die("please declare the consumer key and consumer secret as defined in the documentation");
        }
        $url = 'https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';
        // $url = 'https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';
        $credentials = base64_encode($consumer_key . ':' . $consumer_secret);

        $client = new Client();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Authorization' => ['Basic ' . $credentials]
            ]
        ]);
        return json_decode($response->getBody())->access_token;
    }

    public static function generateSandBoxToken()
    {
        $consumer_key = self::$mpesaConsumerKey;
        $consumer_secret = self::$mpesaConsumerSecret;

        if (!isset($consumer_key) || !isset($consumer_secret)) {
            die("please declare the consumer key and consumer secret as defined in the documentation");
        }
        $url = 'https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';
        $credentials = base64_encode($consumer_key . ':' . $consumer_secret);

        $client = new Client();
        $response = $client->request('GET', $url, [
            'headers' => [
                'Authorization' => ['Basic ' . $credentials]
            ]
        ]);
        return json_decode($response->getBody())->access_token;
    }


    public function STKPushSimulation($BusinessShortCode, $LipaNaMpesaPasskey, $TransactionType, $Amount, $PartyA, $PartyB, $PhoneNumber, $CallBackURL, $AccountReference, $TransactionDesc, $Remark)
    {
        $live = self::$mpesaEnv;
        if ($live == "true") {
            $url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';
            $token = self::generateLiveToken();
        } elseif ($live == "sandbox") {
            $url = 'https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest';
            $token = self::generateSandBoxToken();
        } else {
            return json_encode(["Message" => "invalid application status"]);
        }


        $timestamp = '20' . date("ymdhis");
        $password = base64_encode($BusinessShortCode . $LipaNaMpesaPasskey . $timestamp);

        $curl_post_data = array(
            'BusinessShortCode' => $BusinessShortCode,
            'Password' => $password,
            'Timestamp' => $timestamp,
            'TransactionType' => $TransactionType,
            'Amount' => $Amount,
            'PartyA' => $PartyA,
            'PartyB' => $PartyB,
            'PhoneNumber' => $PhoneNumber,
            'CallBackURL' => $CallBackURL,
            'AccountReference' => $AccountReference,
            'TransactionDesc' => $TransactionType,
            'Remark' => $Remark
        );

        $client = new Client();
        $response = $client->request('POST', $url, [
            'json' => $curl_post_data,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => ['Bearer ' . $token],
            ],
        ]);
        return json_decode($response->getBody());
    }

    public static function STKPushQuery($live, $checkoutRequestID, $businessShortCode, $password, $timestamp)
    {
        $live = self::$mpesaEnv;
        if ($live == "live") {
            $url = 'https://api.safaricom.co.ke/mpesa/stkpushquery/v1/query';
            $token = self::generateLiveToken();
        } elseif ($live == "sandbox") {
            $url = 'https://sandbox.safaricom.co.ke/mpesa/stkpushquery/v1/query';
            $token = self::generateSandBoxToken();
        } else {
            return json_encode(["Message" => "invalid application status"]);
        }

        $timestamp = '20' . date("ymdhis");
        $password = base64_encode($businessShortCode . $password . $timestamp);


        $curl_post_data = array(
            'BusinessShortCode' => $businessShortCode,
            'Password' => $password,
            'Timestamp' => $timestamp,
            'CheckoutRequestID' => $checkoutRequestID
        );
        $client = new Client();
        $response = $client->request('POST', $url, [
            'json' => $curl_post_data,
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => ['Bearer ' . $token],
            ],
        ]);
        return json_decode($response->getBody());
    }

}