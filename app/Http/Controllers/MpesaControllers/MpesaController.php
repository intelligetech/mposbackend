<?php
/**
 * Created by PhpStorm.
 * User: Githire
 * Date: 2/28/2018
 * Time: 12:22 PM
 */

namespace App\Http\Controllers\MpesaControllers;


use App\BillingCycle;
use App\Http\Controllers\Controller;
use App\MpesaPayment;
use App\Status;
use App\StkPush;
use App\Subscription;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Log;

class MpesaController extends Controller
{
    public function mpesaConfirm(Request $request)
    {

        activity('default')
            ->withProperties(compact('request'))
            ->log('request');
        Log::error('b4 request');
        Log::info("me here");
        try {
            $body = $request->input('Body');
            $stkCallBack = $body['stkCallback'];
            $resultCode = $stkCallBack['ResultCode'];
            $merchantRequestID = $stkCallBack['MerchantRequestID'];
            $checkoutRequestID = $stkCallBack['CheckoutRequestID'];
            $resultDescription = $stkCallBack['ResultDesc'];
            $stkPush = StkPush::where('checkoutRequestID', $checkoutRequestID)->first();


            if ($resultCode == 0) {
                /*successful transaction*/
                /*store stkresult, save mpesapayemts and activate subscription*/
                $callBackMetaData = $stkCallBack['CallbackMetadata'];

                $metaDataItem = $callBackMetaData['Item'];

                foreach ($metaDataItem as $dataItem) {
                    switch ($dataItem['Name']) {
                        case 'Amount':
                            $transAmount = $dataItem['Value'];
                            break;
                        case 'MpesaReceiptNumber':
                            $billRefNumber = $dataItem['Value'];
                            break;
                        case 'Balance':
                            if (array_key_exists('Value', $dataItem)) {
                                $orgAccountBalance = $dataItem['Value'];
                            } else {
                                $orgAccountBalance = null;
                            }

                            break;
                        case 'TransactionDate':
                            $transTime = $dataItem['Value'];
                            break;
                        case 'PhoneNumber':
                            $MSISDN = $dataItem['Value'];
                            break;
                        default:
                            break;

                    }
                }

                $stkPush_id = $stkPush->id;
                $stkPushUpdate = $stkPush->update([
                    'resultCode' => $resultCode,
                    'resultDescription' => $resultDescription,
                    'callBackMetaData' => json_encode($callBackMetaData)
                ]);

                $subscription_id = $stkPush->subscription_id;
                $hq_id = $stkPush->hq_id;
                $subscription = Subscription::find($subscription_id);
                $status = Status::where('code', 'SUBACTIVE')->first();
                $status_id = $status->id;
                $billing_cycle_id = $subscription->billing_cycle_id;
                $billCycle = BillingCycle::find($billing_cycle_id);
                $period = $billCycle->name;
                $now = Carbon::now();

                switch ($period) {
                    case 'Yearly':
                        $expiryDate = $now->addYear();
                        break;
                    default:
                        $expiryDate = $now->addYear();
                        break;
                }

                $mpesaPayment = MpesaPayment::create([
                    'hq_id' => $hq_id,
                    'stkpush_id' => $stkPush_id,
                    'subscription_id' => $subscription_id,
                    'TransactionType' => 'LipaNaMpesaOnline',
                    'TransID' => $billRefNumber,
                    'TransTime' => $transTime,
                    'TransAmount' => $transAmount,
                    'BusinessShortCode' => $merchantRequestID,
                    'BillRefNumber' => $billRefNumber,
                    'orgAccountBalance' => $orgAccountBalance,
                    'MSISDN' => $MSISDN,
                    'expiry_date' => $expiryDate
                ]);

                $updateSubscription = $subscription->update([
                    'status_id' => $status_id
                ]);


            } elseif ($resultCode == 1032) {
                /*cancelled transaction*/
                $stkPush_id = $stkPush->id;
                $stkPushUpdate = $stkPush->update([
                    'resultCode' => $resultCode,
                    'resultDescription' => $resultDescription
                ]);
            }

            DB::commit();

        } catch (\Exception $e) {
            Log::info("rollback");
            DB::rollBack();
            $message = $e->getMessage();
            $file = $e->getFile();
            $code = $e->getCode();
            $line = $e->getLine();
            $trace = $e->getTraceAsString();
            $custom = 'Error';
            $user = User::find(1);
            activity('error')
                ->causedBy($user)
                ->withProperties(compact('custom', 'message', 'code', 'file', 'line', 'trace'))
                ->log('exception');

            Log::error(compact('custom', 'message', 'code', 'file', 'line'));
        }

        return 'Completed';
    }
}