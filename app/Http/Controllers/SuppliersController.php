<?php

namespace App\Http\Controllers;

use App\DataTables\SuppliersDataTable;
use Illuminate\Http\Request;

class SuppliersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(SuppliersDataTable $dataTable)
    {
        return $dataTable->render('SuperAdmin.suppliers.index');

    }
}
