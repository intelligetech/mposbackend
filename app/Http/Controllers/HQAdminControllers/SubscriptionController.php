<?php
/**
 * Created by PhpStorm.
 * User: Githire
 * Date: 2/26/2018
 * Time: 11:25 AM
 */

namespace App\Http\Controllers\HQAdminControllers;

use App\BillingCycle;
use App\Http\Controllers\Controller;
use App\Http\Controllers\MpesaControllers\MpesaCustom;
use App\Http\Controllers\UtilityFunctionsController\UtilityFunctionsController;
use App\MpesaPayment;
use App\Status;
use App\StkPush;
use App\Subscription;
use App\User;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Log;

class SubscriptionController extends Controller
{
    protected $mpesaStkPushCallBackURL;
    protected $mpesaBusinessCode;
    protected $mpesaOnlinePassword;
    protected $mpesaTestPhoneNumber;

    public function __construct()
    {
        $this->mpesaStkPushCallBackURL = config('app.mpesaStkPushCallBackURL');
        $this->mpesaBusinessCode = config('app.mpesaBusinessCode');
        $this->mpesaOnlinePassword = config('app.mpesaOnlinePassword');
        $this->mpesaTestPhoneNumber = config('app.mpesaTestPhoneNumber');
       // $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();
        $user_id = $user->id;
        $hq_id = $user->HQID;

        $existingSubscription = Subscription::where('hq_id', $hq_id)
            ->join('status', 'status.id', '=', 'subscriptions.status_id')
            ->where('status_id', '!=', Status::where('code', 'SUBCANCELLED')->first()->id)
            ->where('subscriptions.deleted_at')
            ->select('subscriptions.*', 'status.code')
            ->first();
        if (count($existingSubscription)) {
            $max_id = MpesaPayment::where('subscription_id', $existingSubscription->id)->max('id');
            $payment = MpesaPayment::find($max_id);
        }
        $billingCycle = BillingCycle::first();
        return view('HQAdmin.subscription.index', compact('existingSubscription', 'billingCycle', 'payment'));
    }

    public function index1()
    {
        $BusinessShortCode = '174379';
        $Password = 'bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919';
        $Timestamp = '20180228101010';
        $TransactionType = 'CustomerPayBillOnline';
        $Amount = '1';
        $PartyA = '254724954409';
        $PartyB = '174379';
        $PhoneNumber = '254724954409';
        $CallBackURL = $this->mpesaStkPushCallBackURL;
        $AccountReference = '1232';
        $TransactionDesc = 'TESTING';
        $Remark = 'MYohMY';

        $time = Carbon::now();
        $time = $time->format('Ymdhis');
        $billing_cycle = BillingCycle::where('name', 'Yearly')->first();
        $billingAmount = number_format($billing_cycle->amount);
        $businessShortCode = $this->mpesaBusinessCode;
        $password = $this->mpesaOnlinePassword;
        $Timestamp = $time;
        $transactionType = 'CustomerPayBillOnline';
        $amount = $billingAmount;
        $partyA = $this->mpesaTestPhoneNumber;
        $partyB = $this->mpesaBusinessCode;
        $phoneNumber = $this->mpesaTestPhoneNumber;
        $callBackURL = $this->mpesaStkPushCallBackURL;
        $accountReference = 1;
        $transactionDesc = 'Lyte Shop' . $billing_cycle->name . ' Subscription';
        $remark = 'MYohMY';

        $BusinessShortCode = '174379';
        $CheckoutRequestID = 'ws_CO_28022018102815517';

        /*stkpush*/
        $mpesaCustom = new MpesaCustom();

        try {
            $stkPush = $mpesaCustom->STKPushSimulation(
                $businessShortCode,
                $password,
                $transactionType,
                $amount,
                $partyA,
                $partyB,
                $phoneNumber,
                $callBackURL,
                $accountReference,
                $transactionDesc,
                $remark
            );

            Log::info(json_encode($stkPush));
            dd($stkPush);
            /*store stkpush for verification in callback*/
            $stkPushTable = StkPush::create([
                "merchantRequestID" => $stkPush->MerchantRequestID,
                "checkoutRequestID" => $stkPush->CheckoutRequestID,
                "responseCode" => $stkPush->ResponseCode,
                "responseDescription" => $stkPush->ResponseDescription,
                "customerMessage" => $stkPush->CustomerMessage
            ]);
        } catch (\Exception $e) {
            return $e;
        }


        MpesaCustom::setVariables();
        $res = MpesaCustom::generateSandBoxToken();
        dd($res);


        $user = Auth::user();
        $user_id = $user->id;
        $hq_id = $user->HQID;

        $existingSubscription = Subscription::where('hq_id', $hq_id)
            ->join('status', 'status.id', '=', 'subscriptions.status_id')
            ->where('status_id', '!=', Status::where('code', 'SUBCANCELLED')->first()->id)
            ->select('subscriptions.*', 'status.code')
            ->first();
        return view('HQAdmin.subscription.index', compact('existingSubscription'));

    }

    public function save(Request $request)
    {
        try {
            DB::beginTransaction();
           $user = Auth::user();
           // $user = User::where('username', $request['username'])
             //   ->first();
            $user_id = $user->id;
            $hq_id = $user->HQID;

            $inputPhone = $request->input('phone_number');
              $inputPhone = $user->phone_number;
            if (!count($inputPhone)) {
                $inputPhone = $user->phone_number;
            }

            $inputPhone = UtilityFunctionsController::checkPhoneNumber($inputPhone);

            if (!$inputPhone) {
                $custom = 'Incorrect Phone Number Format. Use 2547********';
                return response()->json(compact('custom'), 400);
            }


            $existingSubscription = Subscription::where('hq_id', $hq_id)
                ->where('status_id', '!=', Status::where('code', 'SUBCANCELLED')->first()->id)
                ->first();

            if (count($existingSubscription)) {
                $custom = 'Existing Cancelled Subscription';
                return response()->json(compact('custom'), 400);
            }

            $time = Carbon::now();
            $time = $time->format('Ymdhis');
            $billing_cycle = BillingCycle::where('name', 'Yearly')->first();
            $billingAmount = number_format($billing_cycle->amount);
            $businessShortCode = $this->mpesaBusinessCode;
            $password = $this->mpesaOnlinePassword;
            $Timestamp = $time;
            $transactionType = 'CustomerPayBillOnline';
            $amount = $billingAmount;
            $partyA = $inputPhone;
            $partyB = $this->mpesaBusinessCode;
            $phoneNumber = $inputPhone;
            $callBackURL = $this->mpesaStkPushCallBackURL;
            $accountReference = $hq_id;
            $transactionDesc = 'Lyte Shop' . $billing_cycle->name . ' Subscription';
            $remark = 'MYohMY';
            $CheckoutRequestID = 'ws_CO_28022018102815517';


            //$stkQuery = $mpesaCustom->STKPushQuery('', $CheckoutRequestID, $BusinessShortCode, $Password, $Timestamp);
            //dd($stkPush);

            if (count($billing_cycle)) {
                $billing_cycle_id = $billing_cycle->id;
            } else {
                $billing_cycle_id = 1;
            }
            $status = Status::where('code', 'SUBPENDING')->first();
            $status_id = $status->id;
            $subscription = Subscription::create([
                'hq_id' => $hq_id,
                'billing_cycle_id' => $billing_cycle_id,
                'status_id' => $status_id,
                'created_by' => $user_id
            ]);
            $subscription_id = $subscription->id;
            /*stkpush*/
            try {
                $mpesaCustom = new MpesaCustom();
                $stkPush = $mpesaCustom->STKPushSimulation(
                    $businessShortCode,
                    $password,
                    $transactionType,
                    $amount,
                    $partyA,
                    $partyB,
                    $phoneNumber,
                    $callBackURL,
                    $accountReference,
                    $transactionDesc,
                    $remark
                );

                Log::info(json_encode($stkPush));

                /*store stkpush for verification in callback*/
                $stkPushTable = StkPush::create([
                    'hq_id' => $hq_id,
                    'subscription_id' => $subscription_id,
                    "merchantRequestID" => $stkPush->MerchantRequestID,
                    "checkoutRequestID" => $stkPush->CheckoutRequestID,
                    "responseCode" => $stkPush->ResponseCode,
                    "responseDescription" => $stkPush->ResponseDescription,
                    "customerMessage" => $stkPush->CustomerMessage
                ]);

            } catch (\Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                $file = $e->getFile();
                $code = $e->getCode();
                $line = $e->getLine();
                $trace = $e->getTraceAsString();
                $custom = 'Error STK';
                return response()->json(compact('custom', 'message', 'code', 'file', 'line', 'e', 'trace'), 400);
            }
            DB::commit();
            $custom = 'Success';
            return response()->json(compact('custom'), 200);
        } catch (\Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            $file = $e->getFile();
            $code = $e->getCode();
            $line = $e->getLine();
            $trace = $e->getTraceAsString();
            $custom = 'Error';
            return response()->json(compact('custom', 'message', 'code', 'file', 'line', 'e', 'trace'), 400);
        }
    }

    public function checkActivation()
    {
        $user = Auth::user();
        $user_id = $user->id;
        $hq_id = $user->HQID;

        $existingSubscription = Subscription::where('hq_id', $hq_id)
            ->join('status', 'status.id', '=', 'subscriptions.status_id')
            ->where('status_id', '=', Status::where('code', 'SUBACTIVE')->first()->id)
            ->select('subscriptions.*', 'status.code')
            ->first();
        if (count($existingSubscription)) {
            $custom = 'refresh the page';
            $message = 'success';
            return response()->json(compact('message'), 200);
        } else {
            $custom = 'show dialog to initiate payment';
            $message = 'error';
            return response()->json(compact('message'), 400);
        }

    }

    public function activate(Request $request)
    {
        $user = Auth::user();
       // $user = User::where('username', $request['username'])
         //   ->first();
        $user_id = $user->id;
        $hq_id = $user->HQID;
        $me = 'jasdhinfuosdj';
        $phoneNumber = $request->input('phone_number');

        try {
            DB::beginTransaction();
            $subscription = Subscription::where('hq_id', $hq_id)
                ->join('status', 'status.id', '=', 'subscriptions.status_id')
                ->where('status_id', '=', Status::where('code', 'SUBPENDING')->first()->id)
                ->select('subscriptions.*', 'status.code')
                ->first();
            $subscription_id = $subscription->id;

            $inputPhone = $request->input('phone_number');


            if (!count($inputPhone)) {
                $inputPhone = $user->phone_number;
            }

            $inputPhone = UtilityFunctionsController::checkPhoneNumber($inputPhone);

            if (!$inputPhone) {
                $custom = 'Incorrect Phone Number Format. Use 2547********';
                return response()->json(compact('custom'), 400);
            }

            $time = Carbon::now();
            $time = $time->format('Ymdhis');
            $billing_cycle = BillingCycle::where('name', 'Yearly')->first();
            $billingAmount = number_format($billing_cycle->amount);
            $businessShortCode = $this->mpesaBusinessCode;
            $password = $this->mpesaOnlinePassword;
            $Timestamp = $time;
            $transactionType = 'CustomerPayBillOnline';
            $amount = $billingAmount;
            $partyA = $inputPhone;
            $partyB = $this->mpesaBusinessCode;
            $phoneNumber = $inputPhone;
            $callBackURL = $this->mpesaStkPushCallBackURL;
            $accountReference = $hq_id;
            $transactionDesc = 'Lyte Shop' . $billing_cycle->name . ' Subscription';
            $remark = 'MYohMY';
            $CheckoutRequestID = 'ws_CO_28022018102815517';

            $mpesaCustom = new MpesaCustom();
            $stkPush = $mpesaCustom->STKPushSimulation(
                $businessShortCode,
                $password,
                $transactionType,
                $amount,
                $partyA,
                $partyB,
                $phoneNumber,
                $callBackURL,
                $accountReference,
                $transactionDesc,
                $remark
            );

            Log::info(json_encode($stkPush));

            /*store stkpush for verification in callback*/
            $stkPushTable = StkPush::create([
                'hq_id' => $hq_id,
                'subscription_id' => $subscription_id,
                "merchantRequestID" => $stkPush->MerchantRequestID,
                "checkoutRequestID" => $stkPush->CheckoutRequestID,
                "responseCode" => $stkPush->ResponseCode,
                "responseDescription" => $stkPush->ResponseDescription,
                "customerMessage" => $stkPush->CustomerMessage
            ]);

            DB::commit();
            $custom = 'Success';
            return response()->json(compact('custom'), 200);
        } catch (\Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            $file = $e->getFile();
            $code = $e->getCode();
            $line = $e->getLine();
            $trace = $e->getTraceAsString();
            $custom = 'Error STK';
            activity('errors')
                ->withProperties(compact('custom', 'message', 'code', 'file', 'line', 'e', 'trace'))
                ->log("activation errors");
            return response()->json(compact('custom', 'message', 'code', 'file', 'line', 'e', 'trace'), 400);
        }
    }
}