<?php

namespace App\Http\Controllers;

use App\payment;
use App\products;
use App\shop;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /*check user role*/
        $user = Auth::user();

        if ($user->role_id == 1) {
            /*hq-admin*/
            $value = $request->session()->get('HQID');
            $shops = shop::where('HQID', $value)->count('id');
            $users = User::where('HQID', $value)->count('id');
            $products = products::where('HQID', $value)->count('id');
            $sales = payment::where('HQID', $value)->count('id');
            $salesreps = payment::leftjoin('users', 'users.local_id', '=', 'payment.user_id')
                ->leftjoin('shop', 'shop.local_id', '=', 'users.shop_id')
                ->select('users.username', 'shop.shop_name', 'payment.*', DB::raw('SUM(payment.amount) as total_cash'))
                ->groupBy('payment.user_id')
                ->where(['payment.HQID' => $value, 'shop.HQID' => $value, 'users.HQID' => $value])
                ->get();
            $total_revenue = payment::where('HQID', $value)->sum('amount');
            return view('home', compact('shops', 'users', 'products', 'sales', 'salesreps', 'total_revenue'));
        } elseif ($user->role_id == 1000) {
            /*super-admin*/
            $value = $request->session()->get('HQID');
            $shops = shop::count('id');
            $users = User::count('id');
            $products = products::count('id');
            $sales = payment::count('id');
            $salesreps = payment::leftjoin('users', 'users.local_id', '=', 'payment.user_id')
                ->leftjoin('shop', 'shop.local_id', '=', 'users.shop_id')
                ->select('users.username', 'shop.shop_name', 'payment.*', DB::raw('SUM(payment.amount) as total_cash'))
                ->groupBy('payment.user_id')
                ->get();
            $total_revenue = payment::sum('amount');
            return view('SuperAdmin.home', compact('shops', 'users', 'products', 'sales', 'salesreps', 'total_revenue'));

        } else {
            /*empty user*/
            Auth::logout();
            return redirect('/login')->with('error', 'Unauthorised Access');
        }
    }
}
