<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Namshi\JOSE\Base64\Base64Encoder;


class LoginController extends Controller
{
    public function postLogin(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');

        $user = User::where(['username' => $username, 'role_id' => 1])->first();

        if ($user == null) {
            return redirect('/')->with('error', 'The user does not exist');
        } else {
            if ($user->password == $password) {
                $request->session()->put('HQID', $user->HQID);
                $request->session()->put('user_id', $user->id);
                $request->session()->put('local_user_id', $user->local_id);

                return redirect('home');
            } else {

                return redirect('/')->with('error', 'The password does not match');
            }

        }
    }
}
