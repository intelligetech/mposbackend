<?php

namespace App\Http\Controllers;

use App\DataTables\AdminSalesReportDataTable;
use App\payment;
use App\shop;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class AdminSalesReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param AdminSalesReportDataTable $dataTable
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function summary(AdminSalesReportDataTable $dataTable, Request $request)
    {
        $query = $dataTable->query();

        $sales = $query->get();

        $shop_names = [];
        $chart_data = [];
        foreach ($sales as $key => $sale) {
            $single_point = [];
            $formatted_date = $sale->date;
            $shop_data = [];
            $single_point[] = $formatted_date;
            $single_point[] = $sale->amount;
            $shop_data["name"] = $sale->shop_name;

            if (array_key_exists($sale->shop_name, $chart_data)) {
                array_push($chart_data[$sale->shop_name]["data"], $single_point);

            } else {
                $shop_data["data"] = [$single_point];
                $chart_data[$sale->shop_name] = $shop_data;
            }
        }
        $temp_array = [];
        foreach ($chart_data as $dat) {
            array_push($temp_array, $dat);
        }

        $chart_data = $temp_array;

        $start_date = Carbon::create()->subMonth();
        $end_date = Carbon::create();

        $sum_query = payment::where('payment.time', '<>', 0);


        if ($request->has('start_date') && $request->get('start_date') && $request->has('end_date') && $request->get('end_date')) {
            $start_date = new Carbon($request->get('start_date'));
            $end_date = new Carbon($request->get('end_date'));
        }
        $displayStartDate = new Carbon($start_date);
        $displayEndDate = new Carbon($end_date);
        $shop_name = 'All Shops';

        $title = 'Total Sales Comparison For Period: ' . $displayStartDate->toFormattedDateString() . ' To: ' . $displayEndDate->toFormattedDateString() . ' For ' . $shop_name;
        $type = 'summary';
        if ($request->has('shop') && $request->get('shop')) {
            $shop_id = $request->get('shop');
            $shop = shop::find($shop_id);
            $hq_id = $shop->HQID;
            $local_id = $shop->local_id;
            $shop_name = $shop->shop_name;
            $type = 'byItem';
            $chart_data = $query->get();
            $title = 'Sales By Item For Period: ' . $displayStartDate->toFormattedDateString() . ' To: ' . $displayEndDate->toFormattedDateString() . ' For ' . $shop_name;
        }

        /*shops*/
        $shops = [];
        return $dataTable->render('HQAdmin.reports.sales.sales_summary', compact('total_sales', 'chart_data', 'start_date', 'end_date', 'shops', 'title', 'type'));
    }

}
