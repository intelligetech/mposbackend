<?php

namespace App\Http\Controllers\Auth;

use App\headquarter;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use function Sodium\compare;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }


    /*manually login users*/

    public function login(Request $request)
    {
        $this->validateLogin($request);


        $username = $request->input('username');
        $password = $request->input('password');

        /*check if hq admin*/
        $user = User::where(['username' => $username, 'role_id' => 1])->first();

        if (!$user) {
            /*check if super admin*/
            $user = User::where(['username' => $username, 'role_id' => 1000])->first();
        }

        if ($user) {
            if ($user->password == $password) {
                $request->session()->put('HQID', $user->HQID);
                $request->session()->put('user_id', $user->id);
                $request->session()->put('local_user_id', $user->local_id);
                Auth::login($user);
                return redirect('home');
            } else {
                return redirect('/')->with('error', 'The password does not match')->withInput();
            }
        } else {
            return redirect('/')->with('error', 'The user does not exist')->withInput();
        }
    }

    public function username()
    {
        return 'username';
    }
}
