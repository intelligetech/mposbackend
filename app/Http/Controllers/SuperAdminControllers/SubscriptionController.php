<?php
/**
 * Created by PhpStorm.
 * User: Githire
 * Date: 2/26/2018
 * Time: 11:25 AM
 */

namespace App\Http\Controllers\SuperAdminControllers;


use App\DataTables\SubscriptionDataTable;
use App\Http\Controllers\Controller;

class SubscriptionController extends Controller
{
    public function __construct()
    {

        $this->middleware('auth');
    }

    public function index(SubscriptionDataTable $dataTable)
    {
        return $dataTable->render('SuperAdmin.subscription.index');

    }
}