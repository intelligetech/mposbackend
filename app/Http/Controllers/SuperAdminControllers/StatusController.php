<?php
/**
 * Created by PhpStorm.
 * User: Githire
 * Date: 2/26/2018
 * Time: 9:55 AM
 */

namespace App\Http\Controllers\SuperAdminControllers;


use App\DataTables\StatusDataTable;
use App\Http\Controllers\Controller;
use App\Status;
use Auth;
use DB;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    public function __construct()
    {

        $this->middleware('auth');
    }

    public function index(StatusDataTable $dataTable)
    {
        return $dataTable->render('SuperAdmin.status.index');

    }

    public function add()
    {
        return view('SuperAdmin.status.add');
    }

    public function save(Request $request)
    {
        $user = Auth::user();
        $user_id = $user->id;
        $this->validate($request, [
            'status_name' => 'required|unique:status,name,NULL,id,deleted_at,NULL',
            'status_code' => 'required|unique:status,code,NULL,id,deleted_at,NULL'
        ]);

        try {
            DB::beginTransaction();
            $name = $request->input('status_name');
            $code = strtoupper($request->input('status_code'));
            $status = Status::create([
                'name' => $name,
                'code' => $code,
                'created_by' => $user_id
            ]);
            DB::commit();
            return redirect()->back()->with('success_message', 'You have added a new status successfully');

        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Error saving data')->withInput();
        }

    }
}