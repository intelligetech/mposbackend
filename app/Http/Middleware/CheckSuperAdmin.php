<?php

namespace App\Http\Middleware;

use App\Role;
use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckSuperAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (User::where('role_id', Role::where('role_name', 'Super Admin')->first()->id)
            ->where('users.id', Auth::user()->id)
            ->exists()) {
            return $next($request);
        } else
            return redirect('home')->withErrors(['Insufficient privileges'])->setStatusCode(401);
    }
}
