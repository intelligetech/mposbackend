<?php namespace App\Http\ViewComposers;

use App\headquarter;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Users\Repository as UserRepository;

class ProfileComposer
{

    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $users;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository $users
     * @return void
     */
    public function __construct()
    {
        // Dependencies automatically resolved by service container...
       // $this->users = $users;
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $user = Auth::user();
        /*check hq */
        $hq = headquarter::find($user->HQID);
        $view->with(compact('user','hq'));
        //$view->with('count', $this->users->count());
    }

}