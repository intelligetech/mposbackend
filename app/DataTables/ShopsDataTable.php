<?php

namespace App\DataTables;

use App\shop;
use App\User;
use Yajra\Datatables\Services\DataTable;

class ShopsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', 'shopsdatatable.action');
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = shop::leftjoin('headquarter', 'headquarter.id', '=', 'shop.HQID')
            ->select('shop.*', 'headquarter.name as hq_name');
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax('')
            ->removeColumn('shop.id')
            ->parameters([
                'dom' => 'lBfrtip',
                'order' => [[0, 'desc']],
                'buttons' => [
                    'create',
                    'export',
                    'print',
                    'reset',
                    'reload',
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => ['data' => 'id', 'name' => 'shop.id'],
            'shop.name' => ['data' => 'shop_name', 'name' => 'shop.shop_name'],
            'address' => ['data' => 'address', 'name' => 'shop.address'],
            'phone_number' => ['data' => 'phone_number', 'name' => 'shop.phone_number'],
            'pin_number' => ['data' => 'pin_number', 'name' => 'shop.pin_number'],
            'headquarter' => ['data' => 'hq_name', 'name' => 'headquarter.name'],
            'created_at' => ['data' => 'created_at', 'name' => 'created_at'],
            'updated_at' => ['data' => 'updated_at', 'name' => 'updated_at']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'shopsdatatable_' . time();
    }
}
