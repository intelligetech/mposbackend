<?php

namespace App\DataTables;

use App\User;
use Yajra\Datatables\Services\DataTable;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('role', function ($user) {
                if ($user->role_id == 1000) {
                    return '<span class="label label-success">Super Admin</span>';
                } elseif ($user->role_id == 1) {
                    return '<span class="label label-primary">HQ Admin</span>';
                } elseif ($user->role_id == 2) {
                    return '<span class="label label-info">Shop Manager</span>';
                } elseif ($user->role_id == 3) {
                    return '<span class="label label-warning">Cashier</span>';
                }
            })->rawColumns(['role']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        /*        $query = User::query()->select($this->getColumns());*/
        $query = User::leftjoin('roles', 'roles.id', '=', 'users.role_id')
            ->leftjoin('headquarter', 'headquarter.id', '=', 'users.HQID')
            ->leftjoin('shop', 'shop.id', '=', 'users.shop_id')
            ->select('users.*', 'headquarter.name as hq_name', 'shop.shop_name');
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax('')
            ->removeColumn('users.id')
            ->parameters([
                'dom' => 'lBfrtip',
                'order' => [[0, 'asc']],
                'buttons' => [
                    'create',
                    'export',
                    'print',
                    'reset',
                    'reload',
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => ['data' => 'id', 'name' => 'users.id'],
            'username' => ['data' => 'username', 'name' => 'users.username'],
            'email' => ['data' => 'email', 'name' => 'users.email'],
            'phone_number' => ['data' => 'phone_number', 'name' => 'users.phone_number'],
            'headquarter' => ['data' => 'hq_name', 'name' => 'headquarter.name'],
            'shop' => ['data' => 'shop_name', 'name' => 'shop.shop_name'],
            'role',
            'created_at' => ['data' => 'created_at', 'name' => 'created_at'],
            'updated_at' => ['data' => 'updated_at', 'name' => 'updated_at']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'usersdatatable_' . time();
    }
}
