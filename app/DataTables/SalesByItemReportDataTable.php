<?php

namespace App\DataTables;

use App\bill;
use App\bill_products;
use App\products;
use App\shop;
use App\User;
use Carbon\Carbon;
use function foo\func;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Services\DataTable;

class SalesByItemReportDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', 'salesbyitemreportdatatable.action');
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $request = $this->request();
        $billQuery = bill::join('payment', function ($join) {
            $join->on('payment.bill_id', '=', 'bill.local_id')
                ->on('payment.HQID', '=', 'bill.HQID')
                ->on('payment.shopID', '=', 'bill.shopID');
        })->select(DB::raw('DISTINCT bill.id, bill.local_id,bill.HQID,bill.shopID'))
            ->toSql();
        //return $this->applyScopes($billQuery);

        $billProdsQuery = bill_products::join(DB::raw("($billQuery) AS bill_ids"), function ($join) {
            $join->on('bill_ids.local_id', '=', 'bill_products.bill_id')
                ->on('bill_products.HQID', '=', 'bill_ids.HQID')
                ->on('bill_products.shopID', '=', 'bill_ids.shopID');
        })->select(DB::raw('DISTINCT bill_products.id, bill_products.local_id, bill_products.product_id, bill_products.qty, bill_products.price, bill_products.price AS amount, bill_products.bill_id, bill_products.created_at, bill_products.HQID,bill_products.shopID'))
            ->toSql();
        $productsQuery = products::join(DB::raw("($billProdsQuery) AS bill_products"), function ($join) {
            $join->on('bill_products.product_id', '=', 'products.local_id')
                ->on('bill_products.HQID', '=', 'products.HQID')
                ->on('bill_products.shopID', '=', 'products.shopID');
        })
            ->join('headquarter', 'headquarter.id', '=', 'bill_products.HQID')
            ->join('shop', function ($join) {
                $join->on('shop.local_id', '=', 'bill_products.shopID')
                    ->on('shop.HQID', '=', 'bill_products.HQID');
            })
            ->select(DB::raw('DISTINCT products.id, products.product_name, sum(bill_products.qty) as qty, products.price as price, sum(bill_products.amount) as total_amount, products.shopID, products.HQID, DATE(bill_products.created_at) as sale_date, headquarter.name as hq_name, shop.shop_name'))
            ->groupby(DB::raw('DATE(bill_products.created_at),products.id,shop.shop_name'));


        $start_date = Carbon::create()->subMonth();
        $end_date = Carbon::create();

        if ($request->has('start_date') && $request->get('start_date') && $request->has('end_date') && $request->get('end_date')) {
            $start_date = new Carbon($request->get('start_date'));
            $end_date = new Carbon($request->get('end_date'));
        }
        $productsQuery->where('bill_products.created_at', '>=', $start_date);
        $productsQuery->where('bill_products.created_at', '<=', $end_date);

        if ($request->has('shop') && $request->get('shop')) {
            $shop_id = $request->get('shop');
            $shop = shop::find($shop_id);

            $hq_id = $shop->HQID;
            $local_id = $shop->local_id;

            $productsQuery->where('bill_products.HQID', $hq_id)
                ->where('bill_products.shopID', $local_id);
        }

        return $this->applyScopes($productsQuery);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax('')
            ->parameters([
                'dom' => 'lBfrtip',
                'order' => [[0, 'desc']],
                'buttons' => [
                    'create',
                    'export',
                    'print',
                    'reset',
                    'reload',
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'product' => ['data' => 'product_name', 'name' => 'products.product_name'],
            'quantity' => ['data' => 'qty', 'name' => 'bill_products.qty'],
            'price' => ['data' => 'price', 'name' => 'products.price'],
            'amount' => ['data' => 'total_amount', 'name' => 'bill_products.price'],
            'headquarter' => ['data' => 'hq_name', 'name' => 'headquarter.name'],
            'shop_name' => ['data' => 'shop_name', 'name' => 'shop.shop_name'],
            'date' => ['data' => 'sale_date', 'name' => 'bill_products.created_at']

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'salesbyitemreportdatatable_' . time();
    }

    public function getTop5()
    {
        $query = $this->query();
        $query->orderBy('total_amount', 'desc');
        $query->limit(5);
        return $query;
    }
}
