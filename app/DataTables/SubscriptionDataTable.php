<?php

namespace App\DataTables;

use App\Subscription;
use Yajra\Datatables\Services\DataTable;

class SubscriptionDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', 'subscriptiondatatable.action');
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Subscription::leftjoin('headquarter', 'headquarter.id', '=', 'subscriptions.hq_id')
            ->leftjoin('status', 'status.id', '=', 'subscriptions.status_id')
            ->leftjoin('mpesa_payments', 'mpesa_payments.subscription_id', '=', 'subscriptions.id')
            ->select(
                'subscriptions.*',
                'headquarter.name as hq_name',
                'status.name as status_name',
                'mpesa_payments.expiry_date'
            );

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax('')
            ->removeColumn('subscriptions.id')
            ->addAction(['width' => '80px'])
            ->parameters([
                'dom' => 'Bfrtip',
                'order' => [[0, 'desc']],
                'buttons' => [
                    'create',
                    'export',
                    'print',
                    'reset',
                    'reload',
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => ['data' => 'id', 'name' => 'subscriptions.id'],
            'headquarter' => ['data' => 'hq_name', 'name' => 'headquarter.name'],
            'status' => ['data' => 'status_name', 'name' => 'status.name'],
            'expiry_date' => ['data' => 'expiry_date', 'name' => 'mpesa_payments.expiry_date'],
            'created_at' => ['data' => 'created_at', 'name' => 'created_at'],
            'updated_at' => ['data' => 'updated_at', 'name' => 'updated_at']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'subscriptiondatatable_' . time();
    }
}
