<?php

namespace App\DataTables;

use App\bill;
use App\bill_products;
use App\payment;
use App\products;
use App\shop;
use App\User;
use Auth;
use Carbon\Carbon;
use DB;
use Yajra\Datatables\Services\DataTable;

class AdminSalesReportDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', 'adminsalesreportdatatable.action');
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $request = $this->request();

        $start_date = Carbon::create()->subMonth();
        $end_date = Carbon::create();

        if ($request->has('start_date') && $request->get('start_date') && $request->has('end_date') && $request->get('end_date')) {
            $start_date = new Carbon($request->get('start_date'));
            $end_date = new Carbon($request->get('end_date'));
        }
        $hq_id = Auth::user()->HQID;
        $query = payment::groupby(DB::raw('DATE(time),payment.shopID,shop.shop_name'))
            ->join('shop', function ($join) {
                $join->on('shop.local_id', '=', 'payment.shopID')
                    ->on('shop.HQID', '=', 'payment.HQID');
            })
            ->where('payment.time', '<>', 0)
            ->where('payment.HQID', $hq_id)
            ->where('payment.time', '>=', $start_date)
            ->where('payment.time', '<=', $end_date)
            ->select(DB::raw('SUM(amount) as amount, DATE(time) as date,shop.shop_name'));

        /*shop is selected get query for sales per item in the shop*/
        if ($request->has('shop') && $request->get('shop')) {
            $shop_id = $request->get('shop');
            $shop = shop::find($shop_id);
            $hq_id = Auth::user()->HQID;
            $shop_id = $shop->local_id;
            $shop_name = $shop->shop_name;

            $query = products::join('bill_products', function ($join) {
                $join->on('products.local_id', '=', 'bill_products.product_id')
                    ->on('bill_products.HQID', '=', 'products.HQID')
                    ->on('bill_products.shopID', '=', 'products.shopID');
            })->join('bill', function ($join) {
                $join->on('bill.local_id', '=', 'bill_products.bill_id')
                    ->on('bill_products.HQID', '=', 'bill.HQID')
                    ->on('bill_products.shopID', '=', 'bill.shopID');
            })->join('payment', function ($join) {
                $join->on('bill.local_id', '=', 'payment.bill_id')
                    ->on('payment.HQID', '=', 'bill.HQID')
                    ->on('payment.shopID', '=', 'bill.shopID');
            })->join('shop', function ($join) {
                $join->on('shop.local_id', '=', 'payment.shopID')
                    ->on('payment.HQID', '=', 'shop.HQID');
            })->where(['payment.shopID' => $shop_id, 'payment.HQID' => $hq_id])
                ->where('payment.time', '<>', 0)
                ->where('payment.time', '>=', $start_date)
                ->where('payment.time', '<=', $end_date)
                ->select(DB::raw('product_name, shop_name, sum(bill_products.qty)   AS qty, products.price AS price, sum(bill_products.price) AS total_amount, DATE(payment.time) AS sale_date, payment.HQID, payment.shopID'))
                ->groupby(DB::raw('DATE(payment.time),products.id,shop.shop_name,products.product_name'));
            /*            return $this->applyScopes($query);*/
        }
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax('')
            ->parameters([
                'dom' => 'Bfrtip',
                'order' => [[5, 'asc']],
                'buttons' => [
                    'create',
                    'export',
                    'print',
                    'reset',
                    'reload',
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'product' => ['data' => 'product_name', 'name' => 'products.product_name'],
            'quantity' => ['data' => 'qty', 'name' => 'bill_products.qty'],
            'price' => ['data' => 'price', 'name' => 'products.price'],
            'amount' => ['data' => 'total_amount', 'name' => 'bill_products.price'],
            'shop_name' => ['data' => 'shop_name', 'name' => 'shop.shop_name'],
            'date' => ['data' => 'sale_date', 'name' => 'payment.time']

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'adminsalesreport_' . time();
    }
}
