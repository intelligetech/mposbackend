<?php

namespace App\DataTables;

use App\payment;
use App\shop;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Services\DataTable;

class SalesReportDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    protected $sum_query = null;

    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', 'salesreportdatatable.action');
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $request = $this->request();

        $start_date = Carbon::create()->subMonth();
        $end_date = Carbon::create();


        $query = payment::groupby(DB::raw('DATE(time)'))
            ->where('payment.time', '<>', 0)
            ->select(DB::raw('SUM(amount) as amount, DATE(time) as date'));


        $query->where('payment.time', '>=', $start_date);
        $query->where('payment.time', '<=', $end_date);

        if ($request->has('shop') && $request->get('shop')) {
            $shop_id = $request->get('shop');
            $shop = shop::find($shop_id);

            $hq_id = $shop->HQID;
            $local_id = $shop->local_id;

            $query->where('payment.HQID', $hq_id)
                ->where('payment.shopID', $local_id);
        }
        return $this->applyScopes($query);
    }


    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax('')
            ->parameters([
                'dom' => 'lBfrtip',
                'order' => [[0, 'desc']],
                'buttons' => [
                    'create',
                    'export',
                    'print',
                    'reset',
                    'reload',
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'date' => ['data' => 'date', 'name' => 'payment.time'],
            'amount' => ['data' => 'amount', 'name' => 'payment.amount'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'salesreportdatatable_' . time();
    }
}
