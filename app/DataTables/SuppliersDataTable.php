<?php

namespace App\DataTables;

use App\supplier;
use App\User;
use Yajra\Datatables\Services\DataTable;

class SuppliersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', 'suppliersdatatable.action');
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = supplier::leftjoin('headquarter', 'headquarter.id', '=', 'supplier.HQID')
            ->leftjoin('shop', 'shop.id', '=', 'supplier.shop_id')
            ->select('supplier.*', 'headquarter.name as hq_name', 'shop.shop_name');
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->removeColumn('supplier.id')
            ->minifiedAjax('')
            ->parameters([
                'dom' => 'lBfrtip',
                'order' => [[0, 'desc']],
                'buttons' => [
                    'create',
                    'export',
                    'print',
                    'reset',
                    'reload',
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => ['data' => 'id', 'name' => 'supplier.id'],
            'supplier_name' => ['data' => 'supplier_name', 'name' => 'supplier.supplier_name'],
            'contact_person' => ['data' => 'contact_person', 'name' => 'supplier.contact_person'],
            'address' => ['data' => 'address', 'name' => 'supplier.address'],
            'phone_number' => ['data' => 'phone_number', 'name' => 'supplier.phone_number'],
            'email' => ['data' => 'email', 'name' => 'supplier.email'],
            'county' => ['data' => 'county', 'name' => 'supplier.county'],
            'headquarter' => ['data' => 'hq_name', 'name' => 'headquarter.name'],
            'shop' => ['data' => 'shop_name', 'name' => 'shop.shop_name'],
            'created_at' => ['data' => 'created_at', 'name' => 'created_at'],
            'updated_at' => ['data' => 'updated_at', 'name' => 'updated_at']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'suppliersdatatable_' . time();
    }
}
