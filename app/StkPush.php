<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StkPush extends Model
{
    protected $table = 'stkpush';
    protected $guarded = ['id'];
}
