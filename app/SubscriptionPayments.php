<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionPayments extends Model
{
    protected $table = 'subscriptionPayments';
    protected $guarded = ['id'];
}
